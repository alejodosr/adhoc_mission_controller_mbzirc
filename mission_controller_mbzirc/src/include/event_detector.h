#ifndef EVENT_DETECTOR_H
#define EVENT_DETECTOR_H

#include <iostream>
#include "state_machine.h"
#include <math.h>
#include "eigen3/Eigen/Eigen"
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseStamped.h>
#include <chrono>
#include <ctime>

class EventDetector
{

public:

  //Constructor.
  EventDetector();

  // Destructor.
  ~EventDetector();

  void setPoses(geometry_msgs::PoseStamped ball_pose,
        geometry_msgs::PoseStamped base_pose,geometry_msgs::PoseStamped drone_far_pose, geometry_msgs::PoseStamped drone_close_pose );
  //void setTime(double time_target_drone_far,double time_target_drone_close,double time_ball);
  void setGripperOffset(Eigen::Vector3d gripperOffset);
  bool generateEvent(Item::State current_state, Item::Event & next_event);
  void printEvent(Item::Event event);
  void setCounterFarTargetDetector(int counter);
  void setCounterCloseTargetDetector(int counter);
  void setCounterCloseBallDetector(int counter);
  void setMotionDirectionFlag(bool value);
  void setBallCatchFlag(bool value);
  void setArrivedToSearchFlag(int flag);
  void setBallTime(double time_ball);
  bool checkTimeOutBallCatch();
  void getCatchCounter(int & counter_catch);




  void startTimerRecover();
  void setTimeRecoverThreshold(int counter);
  bool checkTimeOutRecover();

  void startTimerBallCatchInCatch();
  bool checkTimeOutBallCatchInCatch();


  void activateBallDetectionAllTime(bool activate_ball_detector_all_time);
  double distanceBetweenPosesXY(geometry_msgs::PoseStamped pose_1, geometry_msgs::PoseStamped pose_2);
  double distanceBetweenPosesZ(geometry_msgs::PoseStamped pose_1, geometry_msgs::PoseStamped pose_2);

  int n_min_drone_close_detections;
  int n_min_drone_far_detections;
  int n_min_drone_far_detections_focus;
  int n_min_ball_close_detections;

  bool wait_key_take_off;


  double min_height_take_off;
  geometry_msgs::PoseStamped center_search_behaviour;

  double distance_threshold_catch_DR;

  bool ball_catch_flag;

private:
  bool ACTIVATE_BALL_DETECTOR_ALL_TIME;




    Item::Event last_event;

    geometry_msgs::PoseStamped ball_pose;
    geometry_msgs::PoseStamped base_pose;
    geometry_msgs::PoseStamped drone_far_pose;
    geometry_msgs::PoseStamped drone_close_pose;


    //motion_controller_process::SetPoints planner_points;
    Eigen::Vector3d gripperOffset;

    double time_target_drone_far;
    double time_target_drone_close;
    double time_ball;

    double initial_time_recover;
    double time_elapsed_recover_threshold;

    double initial_time_ball_catch;

    bool motion_direction_flag;
    bool arrived_to_search_flag;
    int counter_target_far;
    int counter_target_close;
    int counter_ball_close;
    int counter_catch;
};
#endif 
