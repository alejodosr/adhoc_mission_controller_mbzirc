#include "behavior_execution.h"

//Constructor
BehaviorExecution::BehaviorExecution(){
  gimbal_angles_flag = false;
  ball_pose_flag = false;
  base_link_flag = false;
  drone_far_flag = false;
  drone_close_flag = false;
  ball_close_flag = false;
  ball_close_flag_NO_kalman = false;
  save_pose_take_off_flag = false;

  //flag disable counters far and close

  far_perception_enable_flag = false;
  close_perception_enable_flag = false;

  //Perception flag
  reset_perception_counters_search = false;

  //Search flags
  search_trajectory_in_progress_flag = false;
  search_trajectory_clockwise_flag = false;
  stop_increasing_altitude_search = false;
  counter_increasing_altitude_search = 0;

  //focus flags
  focus_descending_flag = false;
  focus_achieved_goal_altitude = false;
  focus_trajectory_in_progress_flag = false;
  end_focus_state = false;

  //recover
  recover_turning_around_in_progress_flag = false;

  ACTIVATE_BALL_DETECTOR_ALL_TIME= false;

  gripper_is_close = false;

  dynamic_offset_catch_z = 0;

  ball_seen = false;
  drone_seen = false;

  last_movement_executed = ros::Time::now();
  time_target_drone_close = ros::Time::now().toSec();
  time_ball = ros::Time::now().toSec();
  init_time = ros::Time::now().toSec();

  altitude_search_adhoc_counter = 0;
}

//Destructor
BehaviorExecution::~BehaviorExecution() {
}

//Set poses
void BehaviorExecution::setBallPose(geometry_msgs::PoseStamped ball_pose)
{
  if(!ball_pose_flag){
    ball_pose_flag = true;
  }

  this->ball_pose = ball_pose;
}
void BehaviorExecution::setBaseLinkPose(geometry_msgs::PoseStamped base_link_pose)
{
  if(!base_link_flag){
    base_link_flag = true;
  }
  this->base_pose = base_link_pose;
}
void BehaviorExecution::setDroneFarPose(geometry_msgs::PoseStamped drone_far_pose)
{
  if(!drone_far_flag){
    drone_far_flag = true;
  }
  this->drone_far_pose = drone_far_pose;
}
void BehaviorExecution::setDroneClosePose(geometry_msgs::PoseStamped drone_close_pose)
{
  if(!drone_close_flag){
    drone_close_flag = true;
  }
  this->drone_close_pose = drone_close_pose;
}

void BehaviorExecution::setBallClosePose(geometry_msgs::PoseStamped ball_close_pose)
{
  if(!ball_close_flag){
    ball_close_flag = true;
  }
  this->ball_close_pose = ball_close_pose;
}

void BehaviorExecution::setBallClosePose_NO_kalman(geometry_msgs::PoseStamped ball_close_pose_NO_kalman)
{
  if(!ball_close_flag_NO_kalman){
    ball_close_flag_NO_kalman = true;
  }
  this->ball_close_pose_NO_kalman = ball_close_pose_NO_kalman;
}

void BehaviorExecution::setDroneFuturePose(geometry_msgs::PoseStamped drone_future_pose)
{
  if(!drone_close_flag){
    drone_future_flag = true;
  }
  this->drone_future_pose = drone_future_pose;
}
//Set last time poses were checked
void BehaviorExecution::setBallTime(double time_ball){
  this->time_ball = time_ball;
}
void BehaviorExecution::setDroneFarTime(double time_target_drone_far){
  this->time_target_drone_far = time_target_drone_far;
}
void BehaviorExecution::setDroneCloseTime(double time_target_drone_close){
  this->time_target_drone_close = time_target_drone_close;
}

//Set gripperOffset
void BehaviorExecution::setGripperOffset(Eigen::Vector3d gripperOffset){
  this->gripperOffset = gripperOffset;
}
void BehaviorExecution::setExternalGimbalAltitude(double target_angle,double min_altitude){
  this->TARGET_ANGLE = target_angle;
  this->MIN_ALTITUDE = min_altitude;

}

void BehaviorExecution::setGimbalAngle(geometry_msgs::Vector3Stamped gimbal_state){
  if(!gimbal_angles_flag){
    gimbal_angles_flag = true;
  }
  this->gimbal_angles_ = {gimbal_state.vector.x, gimbal_state.vector.y, gimbal_state.vector.z};
}

//Returns:
// 0 data does not need to be sent
// 1 planner goal points need to be sent
// 2 external yaw need to be sent
// 3 planner goal and external yaw need to be sent
// 4 take off high level command needs to be sent
// 5 land high level command needs to be sent
int BehaviorExecution::executeState(Item::State state, std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed,nav_msgs::Odometry & external_yaw){
  int result = 0;
  int answer = 0;
  switch(state){
  case Item::State::TAKE_OFF:
    //std::cout << "[Executing] TAKE_OFF" << std::endl;
    if (behaviorTakeOff(points_list,inflation_radius,gear_speed)){
      result = 4;
    }else {
      result = 0;
    }
    break;
  case Item::State::SEARCH:
    //std::cout << "[Executing] SEARCHING" << std::endl;

    publishExternalYaw(state, external_yaw);

//    if(behaviorSearchDrone(points_list,inflation_radius,gear_speed)){ // Return true if a new trayectory is needed
//      result = 3;
//    }else{
//      result = 2;
//    }

    answer = behaviorSearchDrone(points_list,inflation_radius,gear_speed);

    if( answer == 1){
      result = 3;
    }else if (answer == 2){
      result = 6;
    } else{
      result = 2;
    }
    break;
  case Item::State::FOCUS:
    //std::cout << "[Executing] FOCUS" << std::endl;

    publishExternalYaw(state, external_yaw);
    if(behaviorFocus(points_list,inflation_radius,gear_speed)){
      result = 3;
    } else{
      result = 2;
    }

    break;
  case Item::State::RECOVER:
    //std::cout << "[Executing] SEARCHING" << std::endl;

    publishExternalYaw(state, external_yaw);
    if(behaviorRecover(points_list,inflation_radius,gear_speed, external_yaw)){ // Return true if a new trayectory is needed
      result = 3;
    }else{
      result = 2;
    }

    break;
  case Item::State::FOLLOW_DRONE_CLOSE:
    //std::cout << "[Executing] Follow drone close" << std::endl;


    publishExternalYaw(state, external_yaw);
    if(behaviorFollowDroneClose(points_list,inflation_radius,gear_speed)){
      result = 3;
    }else{
      result = 2;
    }
    break;
  case Item::State::FOLLOW_DRONE_FAR:
    //std::cout << "[Executing] Follow drone far" << std::endl;

    publishExternalYaw(state, external_yaw);
    behaviorFollowDroneFar(points_list,inflation_radius,gear_speed);
    result = 3;
    break;
  case Item::State::INTERCEPTION:
    //std::cout << "[Executing] Interception" << std::endl;
    //            behaviorInterceptDrone(points_list,inflation_radius,gear_speed);
    //            result = 3;
    break;
  case Item::State::FOLLOW_BALL:
    //std::cout << "[Executing] Follow drone close" << std::endl;
    publishExternalYaw(state, external_yaw);
    if(behaviorFollowBall(points_list,inflation_radius,gear_speed)){
      result = 3;
    }else{
      result = 2;
    }
    break;
  case Item::State::CATCH_BALL:
    //std::cout << "[Executing] Catch ball" << std::endl;
    answer = behaviorCatchBall(points_list,inflation_radius,gear_speed);

    if( answer == 1){
      result = 3;
    }else if (answer == 2){
      result = 6;
    } else{
      result = 2;
    }
    break;
  case Item::State::LAND:
    //std::cout << "[Executing] LAND" << std::endl;
    if(behaviorLand(points_list,inflation_radius,gear_speed)){
      result = 5;
    }else{
      result = 3;
    }
    break;
  }
  return result;
}

//Returns:
// 0 do nothing
// 1 enable far and close
// 2 disable and reset far and close
// 3 disable far and enable close
// 4 start search behavior

int BehaviorExecution::resetFlags(Item::State state, bool & far_counter, bool & close_counter, bool & ball_counter){
  int result = 0;
  switch(state){
  case Item::State::START_STATE:
    //        if(disablePerception()){
    //            result = 2;
    //        }

    far_counter = false;
    close_counter = false;
    ball_counter = false;

    break;
  case Item::State::TAKE_OFF:
    resetFocusFlags();
    search_trajectory_in_progress_flag = false;

    far_counter = false;
    close_counter = false;
    ball_counter = false;

    break;
  case Item::State::SEARCH:
    far_counter = true;
    close_counter = true;
    if(ACTIVATE_BALL_DETECTOR_ALL_TIME){
      ball_counter = true;
    }else{
      ball_counter = false;
    }
    break;
  case Item::State::FOCUS:
    search_trajectory_in_progress_flag = false;
    recover_first_time_call_flag = false;
    //       std::cout << "focus_descending_flag"<< focus_descending_flag << std::endl;
    //       std::cout << "focus_trajectory_in_progress_flag"<< focus_trajectory_in_progress_flag << std::endl;

    if(focus_descending_flag && !focus_trajectory_in_progress_flag){
      far_counter = false;
    }
    if(focus_descending_flag && focus_trajectory_in_progress_flag){
      far_counter = true;
    }
    if(end_focus_state){
      far_counter = true;

      result = 1;
      end_focus_state = false;
    }
    close_counter = true;

    if(ACTIVATE_BALL_DETECTOR_ALL_TIME){
      ball_counter = true;
    }else{
      ball_counter = false;
    }
    far_counter = false;

    break;
  case Item::State::FOLLOW_DRONE_CLOSE:
    far_counter = false;
    close_counter = true;
    ball_counter = true;
    break;
  case Item::State::FOLLOW_DRONE_FAR:
    resetFocusFlags();
    far_counter = true;
    close_counter = true;
    ball_counter = true;


    break;
    //     case Item::State::INTERCEPTION:
    //        search_trajectory_in_progress_flag = false;
    //        reset_perception_counters_search = false;

    //        far_counter = true;
    //        close_counter = true;

  case Item::State::RECOVER:
    resetFocusFlags();
    focus_trajectory_in_progress_flag = false;
    search_trajectory_in_progress_flag = false;


    if(recover_turning_around_in_progress_flag){
      far_counter = false;
    }
    if(!recover_turning_around_in_progress_flag){
      far_counter = true;
    }
    close_counter = true;

    if(ACTIVATE_BALL_DETECTOR_ALL_TIME){
      ball_counter = true;
    }else{
      ball_counter = false;
    }
    far_counter= false;

    break;

  case Item::State::FOLLOW_BALL:
    far_counter = false;
    close_counter = true;
    ball_counter = true;


    break;
    break;
  case Item::State::CATCH_BALL:
    far_counter = false;
    close_counter = true;
    ball_counter = true;


    break;
  case Item::State::LAND:
    far_counter = false;
    close_counter = false;
    ball_counter = false;

    break;
  }
  return result;
}




int BehaviorExecution::behaviorSearchDrone(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed){

  double CLOSE_FINGERS_AND_PRAY = 1.0;

  if (ball_seen && (distanceBetweenPoses(ball_close_pose_NO_kalman,base_pose) < 2.0) &&
      (std::fabs((ros::Time::now().toSec() - time_ball)) < 2.0)){

    std::cout << "High frequency maneuver" << std::endl;

    bool close_gripper = false;
    if (distanceBetweenPoses(ball_close_pose_NO_kalman,base_pose) < CLOSE_FINGERS_AND_PRAY) {
      close_gripper = true;
    }

    if (tent_strategy &&
        (base_pose.pose.position.x > ball_close_pose_NO_kalman.pose.position.x)){

      if (time_ball != behaviorCatchBallClose_lastTime){
        behaviorCatchBallClose_lastTime = time_ball;

        std::cout << "Moving to target in high frequency" << std::endl;

        points_list.clear();
        geometry_msgs::PoseStamped first_point;
        first_point.pose.position.x = ball_close_pose_NO_kalman.pose.position.x - offset_predictive;
        if (first_point.pose.position.x > 32.0){
          first_point.pose.position.x = 32.0;
        }

        if (first_point.pose.position.x < 17.5){
          first_point.pose.position.x = 17.5;
        }

        first_point.pose.position.y = y_quad;
        first_point.pose.position.z = base_pose.pose.position.z;
        points_list.push_back(first_point);

        if (close_gripper){
          std::cout << "CLOSING GRIPPER AND HIGH FREQUENCY" << std::endl;
          return 2;
        }
        else{
          std::cout << "HIGH FREQUENCY" << std::endl;
          return 1;
        }
      }
      else{
        return 0;
      }
    }
    else if (!tent_strategy &&
             (base_pose.pose.position.x < ball_close_pose_NO_kalman.pose.position.x)){

      if (time_ball != behaviorCatchBallClose_lastTime){
        behaviorCatchBallClose_lastTime = time_ball;

        std::cout << "Moving to target in high frequency in " << ros::Time::now().toSec()  - init_time << std::endl;

        points_list.clear();
        geometry_msgs::PoseStamped first_point;
        first_point.pose.position.x = ball_close_pose_NO_kalman.pose.position.x + offset_predictive;
        if (first_point.pose.position.x > 22.5){
          first_point.pose.position.x = 22.5;
        }

        if (first_point.pose.position.x < 8.0){
          first_point.pose.position.x = 8.0;
        }

        first_point.pose.position.y = y_quad;
        first_point.pose.position.z = base_pose.pose.position.z;
        points_list.push_back(first_point);

        if (close_gripper){
          return 2;
        }
        else{
          return 1;
        }
      }
      else{
        return 0;
      }

    }
    else{
      return 0;
    }
  }
  else if (ball_seen &&
           (std::fabs((ros::Time::now().toSec() - time_ball)) > 11.0) &&
           (std::fabs((ros::Time::now().toSec() - last_movement_executed.toSec())) > 10.0)){
    last_movement_executed = ros::Time::now();

    std::cout << "BALL maneuver in " << ros::Time::now().toSec()  - init_time <<  std::endl;
    std::cout << "Ball position x: " << ball_close_pose_NO_kalman.pose.position.x << std::endl;
    std::cout << "Ball position y: " << ball_close_pose_NO_kalman.pose.position.y << std::endl;
    std::cout << "Ball position z: " << ball_close_pose_NO_kalman.pose.position.z << std::endl;

    // temp variable
    geometry_msgs::PoseStamped target_pose;

    target_pose.pose.position.x = ball_close_pose_NO_kalman.pose.position.x;
    target_pose.pose.position.y = y_quad;
    target_pose.pose.position.z = ball_close_pose_NO_kalman.pose.position.z;

    if ((ball_close_pose_NO_kalman.pose.position.z - base_pose.pose.position.z) > 2.0){
      target_pose.pose.position.z = base_pose.pose.position.z + 2.0;
    }

    if ((ball_close_pose_NO_kalman.pose.position.z - base_pose.pose.position.z) < -2.0){
      target_pose.pose.position.z = base_pose.pose.position.z - 2.0;
    }

    if ((ball_close_pose_NO_kalman.pose.position.x - base_pose.pose.position.x) > 2.0){
      target_pose.pose.position.x = base_pose.pose.position.x + 2.0;
    }

    if ((ball_close_pose_NO_kalman.pose.position.x - base_pose.pose.position.x) < -2.0){
      target_pose.pose.position.x = base_pose.pose.position.x - 2.0;
    }

    if (tent_strategy){
      std::uniform_real_distribution<double> distribution(-offset_predictive_width,offset_predictive_width);
      double generated_offset = distribution(generator);

      points_list.clear();
      geometry_msgs::PoseStamped first_point;
      first_point.pose.position.x = target_pose.pose.position.x - offset_predictive + generated_offset;
      if (first_point.pose.position.x > 32.0){
        first_point.pose.position.x = 32.0;
      }

      if (first_point.pose.position.x < 17.5){
        first_point.pose.position.x = 17.5;
      }

      first_point.pose.position.y = target_pose.pose.position.y;
      first_point.pose.position.z = target_pose.pose.position.z - gripper_offset_adjust;
      points_list.push_back(first_point);

      std::cout << "Generated offset: " << generated_offset << std::endl;
      std::cout << "Base link position x: " << base_pose.pose.position.x << std::endl;
      std::cout << "Base link position y: " << base_pose.pose.position.y << std::endl;
      std::cout << "Base link position z: " << base_pose.pose.position.z << std::endl;
      std::cout << "Target Ball position x: " << first_point.pose.position.x << std::endl;
      std::cout << "Target Ball position y: " << first_point.pose.position.y << std::endl;
      std::cout << "Target Ball position z: " << first_point.pose.position.z << std::endl;

    }
    else{
      std::uniform_real_distribution<double> distribution(-offset_predictive_width,offset_predictive_width);
      double generated_offset = distribution(generator);

      points_list.clear();
      geometry_msgs::PoseStamped first_point;
      first_point.pose.position.x = target_pose.pose.position.x  + offset_predictive + generated_offset;

      if (first_point.pose.position.x > 22.5){
        first_point.pose.position.x = 22.5;
      }

      if (first_point.pose.position.x < 8.0){
        first_point.pose.position.x = 8.0;
      }


      first_point.pose.position.y = target_pose.pose.position.y;
      first_point.pose.position.z = target_pose.pose.position.z - gripper_offset_adjust;
      points_list.push_back(first_point);

      std::cout << "Generated offset: " << generated_offset << std::endl;
      std::cout << "Base link position x: " << base_pose.pose.position.x << std::endl;
      std::cout << "Base link position y: " << base_pose.pose.position.y << std::endl;
      std::cout << "Base link position z: " << base_pose.pose.position.z << std::endl;
      std::cout << "Target Ball position x: " << first_point.pose.position.x << std::endl;
      std::cout << "Target Ball position y: " << first_point.pose.position.y << std::endl;
      std::cout << "Target Ball position z: " << first_point.pose.position.z << std::endl;
    }

    return 1;
  }
  else if(drone_seen &&
          (std::fabs((ros::Time::now().toSec() - time_target_drone_close)) > 11.0) &&
          (std::fabs((ros::Time::now().toSec() - last_movement_executed.toSec())) > 10.0)){
    last_movement_executed = ros::Time::now();

    std::cout << "DRONE maneuver in " << ros::Time::now().toSec() - init_time << std::endl;


    // temp variable
    geometry_msgs::PoseStamped target_pose;

    target_pose.pose.position.x = drone_close_pose.pose.position.x;
    target_pose.pose.position.y = y_quad;
    target_pose.pose.position.z = drone_close_pose.pose.position.z - 2.0;

    if ((drone_close_pose.pose.position.z  - 2.0 - base_pose.pose.position.z) > 2.0){
      target_pose.pose.position.z = base_pose.pose.position.z + 2.0;
    }

    if ((drone_close_pose.pose.position.z - 2.0 - base_pose.pose.position.z) < -2.0){
      target_pose.pose.position.z = base_pose.pose.position.z - 2.0;
    }

    if ((drone_close_pose.pose.position.x - base_pose.pose.position.x) > 2.0){
      target_pose.pose.position.x = base_pose.pose.position.x + 2.0;
    }

    if ((drone_close_pose.pose.position.x - base_pose.pose.position.x) < -2.0){
      target_pose.pose.position.x = base_pose.pose.position.x - 2.0;
    }

    if (tent_strategy){
      std::uniform_real_distribution<double> distribution(-offset_predictive_width,offset_predictive_width);
      double generated_offset = distribution(generator);

      points_list.clear();
      geometry_msgs::PoseStamped first_point;
      first_point.pose.position.x = target_pose.pose.position.x - offset_predictive + generated_offset;
      if (first_point.pose.position.x > 32.0){
        first_point.pose.position.x = 32.0;
      }

      if (first_point.pose.position.x < 17.5){
        first_point.pose.position.x = 17.5;
      }


      first_point.pose.position.y = target_pose.pose.position.y;
      first_point.pose.position.z = target_pose.pose.position.z - gripper_offset_adjust;
      points_list.push_back(first_point);

      std::cout << "Generated offset: " << generated_offset << std::endl;
      std::cout << "Base link position x: " << base_pose.pose.position.x << std::endl;
      std::cout << "Base link position y: " << base_pose.pose.position.y << std::endl;
      std::cout << "Base link position z: " << base_pose.pose.position.z << std::endl;
      std::cout << "Target Drone position x: " << first_point.pose.position.x << std::endl;
      std::cout << "Target Drone position y: " << first_point.pose.position.y << std::endl;
      std::cout << "Target Drone position z: " << first_point.pose.position.z << std::endl;
    }
    else{
      std::uniform_real_distribution<double> distribution(-offset_predictive_width,offset_predictive_width);
      double generated_offset = distribution(generator);

      points_list.clear();
      geometry_msgs::PoseStamped first_point;
      first_point.pose.position.x = target_pose.pose.position.x + offset_predictive + generated_offset;
      if (first_point.pose.position.x > 22.5){
        first_point.pose.position.x = 22.5;
      }

      if (first_point.pose.position.x < 8.0){
        first_point.pose.position.x = 8.0;
      }


      first_point.pose.position.y = target_pose.pose.position.y;
      first_point.pose.position.z = target_pose.pose.position.z - gripper_offset_adjust;
      points_list.push_back(first_point);

      std::cout << "Generated offset: " << generated_offset << std::endl;
      std::cout << "Base link position x: " << base_pose.pose.position.x << std::endl;
      std::cout << "Base link position y: " << base_pose.pose.position.y << std::endl;
      std::cout << "Base link position z: " << base_pose.pose.position.z << std::endl;
      std::cout << "Target Drone position x: " << first_point.pose.position.x << std::endl;
      std::cout << "Target Drone position y: " << first_point.pose.position.y << std::endl;
      std::cout << "Target Drone position z: " << first_point.pose.position.z << std::endl;
    }

    return 1;
  }
  else if (std::fabs((ros::Time::now().toSec() - last_movement_executed.toSec())) > 30.0){
    last_movement_executed = ros::Time::now();

    std::cout << "SEARCH maneuver in " << ros::Time::now().toSec()  - init_time << std::endl;

    if (tent_strategy){
      points_list.clear();
      geometry_msgs::PoseStamped first_point;
      first_point.pose.position.x = 20.0 + offset_x_search;
      first_point.pose.position.y = y_quad;
      first_point.pose.position.z = ALTITUDE_SEARCH + (double) altitude_search_adhoc_counter * 0.5;
      if (first_point.pose.position.z > 15.0){
        first_point.pose.position.z = 15.0;
      }
      points_list.push_back(first_point);

      std::cout << "Automatic altitude: " << first_point.pose.position.z << std::endl;
      std::cout << "Automatic position x: " << first_point.pose.position.x << std::endl;
      std::cout << "Automatic position y: " << first_point.pose.position.y << std::endl;
      std::cout << "Automatic position z: " << first_point.pose.position.z << std::endl;

      altitude_search_adhoc_counter++;
    }
    else{
      points_list.clear();
      geometry_msgs::PoseStamped first_point;
      first_point.pose.position.x = 20.0 - offset_x_search;
      first_point.pose.position.y = y_quad;
      first_point.pose.position.z = ALTITUDE_SEARCH + (double) altitude_search_adhoc_counter * 0.5;
      if (first_point.pose.position.z > 15.0){
        first_point.pose.position.z = 15.0;
      }

      points_list.push_back(first_point);

      std::cout << "Automatic altitude: " << first_point.pose.position.z << std::endl;
      std::cout << "Automatic position x: " << first_point.pose.position.x << std::endl;
      std::cout << "Automatic position y: " << first_point.pose.position.y << std::endl;
      std::cout << "Automatic position z: " << first_point.pose.position.z << std::endl;

      altitude_search_adhoc_counter++;
    }

    return 1;
  }
  else{
//    std::cout << "Waiting to actuate" << std::endl;
    return 0;
  }
}


///

//double DISTANCE_THRESHOLD = 1;


//if(!search_trajectory_in_progress_flag){

//  search_trajectory_in_progress_flag =true;
//  getTrajectorySearch(points_list);

//#ifdef SEARCH_VARIABLE_ALTITUDE
//  if (!stop_increasing_altitude_search){
//    counter_increasing_altitude_search++;
//  }
//  std::cout << "counter_increasing_altitude_search :"<< counter_increasing_altitude_search << std::endl;
//#endif

//  last_points_list_search = points_list;

//  inflation_radius = -1.0;
//  gear_speed = gear_speed_search_DR;
//  return true;
//} else {


//if(distanceBetweenPoses(last_points_list_search.back(),base_pose)< DISTANCE_THRESHOLD){
//  search_trajectory_in_progress_flag = false;
//  std::cout << "********************* behaviorSearchDrone DONE ******************" << std::endl;
//}

//return false;
//}

//}

bool BehaviorExecution::behaviorFocus(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed){

  //    double ALTITUDE_FOCUS = ALTITUDE_SEARCH - 1.5;
  //    double THRESHOLD_ALTITUDE = 1;
  //    double THRESHOLD_DISTANCE_SEARCH = 2;


  //    double distance_to_altitude_goal = base_pose.pose.position.z - ALTITUDE_FOCUS;

  //    if(distance_to_altitude_goal < THRESHOLD_ALTITUDE ){
  //        focus_achieved_goal_altitude = true;
  //    }


  //    //********************

  //    if(!focus_descending_flag){
  //        focus_descending_flag = true;
  //        points_list.clear();
  //        geometry_msgs::PoseStamped first_point;
  //        first_point.pose.position.x = base_pose.pose.position.x;
  //        first_point.pose.position.y = base_pose.pose.position.y;
  //        first_point.pose.position.z = ALTITUDE_FOCUS;
  //        points_list.push_back(first_point);
  //        inflation_radius = -1.0;
  //        gear_speed = gear_speed_focus_DR;
  //        std::cout << "********************* DECREASING ALTITUDE ******************" << std::endl;

  //        return true;

  //    }else if(focus_trajectory_in_progress_flag){
  //        std::cout << "distanceBetweenPoses(focus_end_point,base_pose)" << distanceBetweenPoses(focus_end_point,base_pose) << std::endl;


  //        if(distanceBetweenPosesXY(focus_end_point,base_pose)<THRESHOLD_DISTANCE_SEARCH){
  //            std::cout << "********************* CHANGING TO SEARCH ******************" << std::endl;

  //            end_focus_state = true;
  //            return false;
  //        }

  //    }
  //    if(focus_descending_flag  && focus_achieved_goal_altitude) {

  //        //if(focus_trajectory_in_progress_flag == false){
  //            std::cout << "********************* ALREADY ALTITUDE ******************" << std::endl;

  //            focus_trajectory_in_progress_flag = true;
  //            reset_perception_counters_search = true;
  //            std::vector<geometry_msgs::PoseStamped> aux;
  //            getTrajectorySearch(aux);
  //            points_list.clear();
  //            geometry_msgs::PoseStamped first_point;
  //            first_point = aux.front();
  //            first_point.pose.position.z = ALTITUDE_FOCUS;
  //            std::cout << "Trajectory focus point: " << first_point.pose.position.x << ", " << first_point.pose.position.y << ", "<< first_point.pose.position.z << std::endl;
  //	    points_list.push_back(first_point);
  //            inflation_radius = -1.0;
  //            gear_speed = gear_speed_focus_DR;
  //            focus_end_point = first_point;
  //            std::cout << "********************* ALREADY ALTITUDE ******************" << std::endl;


  //            return true;

  //        //}

  //    }else{
  //        return false;
  //    }

  std::cout << "FOCUS enteredddddddddd!!" << std::endl;

}

bool BehaviorExecution::behaviorRecover(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed, nav_msgs::Odometry  external_yaw){

  //    double THRESHOLD_YAW = 0.1;

  //    double remaining_yaw = last_external_yaw - external_yaw.pose.pose.orientation.z;

  //    if(!recover_first_time_call_flag){
  //        recover_first_time_call_flag = true;
  //        recover_turning_around_in_progress_flag = true;
  //        external_yaw.pose.pose.orientation.z = last_external_yaw + M_PI;   //Turn around

  //        points_list.clear();
  //        geometry_msgs::PoseStamped first_point;
  //        first_point.pose.position.x = base_pose.pose.position.x;
  //        first_point.pose.position.y = base_pose.pose.position.y;
  //        first_point.pose.position.z = base_pose.pose.position.z;
  //        points_list.push_back(first_point);

  //        inflation_radius = -1.0;
  //        gear_speed = gear_speed_recover_DR;
  //    } else if(recover_turning_around_in_progress_flag && (remaining_yaw < THRESHOLD_YAW)){
  //        recover_turning_around_in_progress_flag = false;
  //    }

  std::cout << "RECOVER enteredddddddddd!!" << std::endl;
}


void BehaviorExecution::behaviorFollowDroneFar(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed)
{

  //    double ALTITUDE_FAR = 5;

  //    Eigen::Vector3d normal_vector;
  //    double module = distanceBetweenPoses(drone_far_pose,base_pose);

  //    normal_vector(0) = (drone_far_pose.pose.position.x - base_pose.pose.position.x)/module;
  //    normal_vector(1) = (drone_far_pose.pose.position.y - base_pose.pose.position.y)/module;
  //    normal_vector(2) = 0;//(drone_far_pose.pose.position.z - base_pose.pose.position.z)/module;

  //    double DISTANCE_FOLLOWING = 2; // Increase to avoid turning in Yaw when close to the last point

  //    points_list.clear();

  //    geometry_msgs::PoseStamped first_point;
  //    first_point.pose.position.x = drone_far_pose.pose.position.x - DISTANCE_FOLLOWING * normal_vector(0);
  //    first_point.pose.position.y = drone_far_pose.pose.position.y - DISTANCE_FOLLOWING * normal_vector(1);
  //    first_point.pose.position.z = base_pose.pose.position.z;//ALTITUDE_FAR;//drone_far_pose.pose.position.z - DISTANCE_FOLLOWING * normal_vector(2);
  ////    points_list.push_back(first_point);


  //#ifdef DONKEY_STATEGY
  //    donkeyStrategySaturation(first_point, 2);
  //#endif

  //    // Check distance
  //    safeDistanceWithNetSaturation(first_point);


  //    points_list.push_back(first_point);

  //    inflation_radius = -1.0; //Disable obstacle avoidance
  //    gear_speed = gear_speed_far_DR;

  std::cout << "FOLLOW DRONE FAR enteredddddddddd!!" << std::endl;

}


void BehaviorExecution::behaviorInterceptDrone(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed)
{
  //    Eigen::Vector3d normal_vector;

  //    double module = distanceBetweenPoses(drone_far_pose,base_pose);

  //    normal_vector(0) = (drone_future_pose.pose.position.x - base_pose.pose.position.x)/module;
  //    normal_vector(1) = (drone_future_pose.pose.position.y - base_pose.pose.position.y)/module;
  //    normal_vector(2) = (drone_future_pose.pose.position.z - base_pose.pose.position.z)/module;

  //    double DISTANCE_FOLLOWING = 1;

  //    points_list.clear();

  //    geometry_msgs::PoseStamped first_point;
  //    first_point.pose.position.x = drone_far_pose.pose.position.x - DISTANCE_FOLLOWING * normal_vector(0);
  //    first_point.pose.position.y = drone_far_pose.pose.position.y - DISTANCE_FOLLOWING * normal_vector(1);
  //    first_point.pose.position.z = drone_far_pose.pose.position.z - DISTANCE_FOLLOWING * normal_vector(2);
  ////    points_list.push_back(first_point);

  ////#ifdef DONKEY_STATEGY
  ////    donkeyStrategySaturation( first_point);
  ////#endif
  //    points_list.push_back(first_point);

  //    inflation_radius = 5.0;

  //    double actual_distance = distanceBetweenPoses(drone_far_pose,base_pose);
  //    gear_speed = calculateGearSpeed(previuos_distance_drone_close, actual_distance);
  //    previuos_distance_drone_close = actual_distance;

  std::cout << "INTERCEPT enteredddddddddd!!" << std::endl;

}
bool BehaviorExecution::behaviorFollowDroneClose(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed)
{
  //    if (time_target_drone_close != behaviorFollowDroneClose_lastTime){

  //        behaviorFollowDroneClose_lastTime = time_target_drone_close;

  //        double OFFSET_ALTITUDE_CLOSE = close_follow_distance_Z_drone_DR;


  //        Eigen::Vector3d normal_vector;

  //        double module = distanceBetweenPosesXY(drone_close_pose, base_pose);

  //        normal_vector(0) = 0;//(drone_close_pose.pose.position.x - base_pose.pose.position.x)/module;
  //        normal_vector(1) = (drone_close_pose.pose.position.y - base_pose.pose.position.y)/ fabs((drone_close_pose.pose.position.y - base_pose.pose.position.y));//);(drone_close_pose.pose.position.y - base_pose.pose.position.y)/module;
  //        normal_vector(2) = 0;//(drone_close_pose.pose.position.z - base_pose.pose.position.z)/module;

  //        double DISTANCE_FOLLOWING = close_follow_distance_XY_drone_DR;

  //        points_list.clear();
  //        geometry_msgs::PoseStamped first_point;
  //        first_point.pose.position.x = drone_close_pose.pose.position.x;//drone_close_pose.pose.position.x - DISTANCE_FOLLOWING * normal_vector(0);
  //        first_point.pose.position.y = drone_close_pose.pose.position.y - DISTANCE_FOLLOWING * normal_vector(1);
  //        first_point.pose.position.z = 8;//drone_close_pose.pose.position.z - DISTANCE_FOLLOWING * normal_vector(2) - OFFSET_ALTITUDE_CLOSE;
  //    //    points_list.push_back(first_point);


  //    #ifdef ADJUST_ALTTITUDE
  //    if(gimbal_angles_flag){
  //        adjustAltitudeWithPerception(first_point);
  //    }
  //    #endif

  //    #ifdef DONKEY_STATEGY
  //        donkeyStrategySaturation( first_point, 1);
  //    #endif

  //      // Check distance
  //      safeDistanceWithNetSaturation(first_point);

  //      points_list.push_back(first_point);

  //      double actual_distance = distanceBetweenPoses(drone_far_pose,base_pose);
  //      inflation_radius = 2;
  //      gear_speed = calculateGearSpeed(previuos_distance_drone_close, actual_distance);
  //      previuos_distance_drone_close = actual_distance;
  //        return true;
  //    }else{
  //        return false;
  //    }

  std::cout << "FOLLOW DRONE CLOSE enteredddddddddd!!" << std::endl;

}

bool BehaviorExecution::behaviorFollowBall(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed)
{
  //    if (time_ball != behaviorFollowBallClose_lastTime){
  //    behaviorFollowBallClose_lastTime = time_ball;

  //    double OFFSET_ALTITUDE_CLOSE = close_follow_distance_Z_ball_DR;

  //    Eigen::Vector3d normal_vector;

  //    double module = distanceBetweenPosesXY(ball_pose, base_pose);

  //    normal_vector(0) = 0;//(ball_pose.pose.position.x - base_pose.pose.position.x)/module;
  //    normal_vector(1) = (ball_pose.pose.position.y - base_pose.pose.position.y)/fabs(ball_pose.pose.position.y - base_pose.pose.position.y);//module;
  //    normal_vector(2) = 0;//(drone_close_pose.pose.position.z - base_pose.pose.position.z)/module;

  //    double DISTANCE_FOLLOWING = close_follow_distance_XY_ball_DR;

  //    points_list.clear();
  //    geometry_msgs::PoseStamped first_point;
  //    first_point.pose.position.x = ball_pose.pose.position.x;// - DISTANCE_FOLLOWING * normal_vector(0);
  //    first_point.pose.position.y = ball_pose.pose.position.y - DISTANCE_FOLLOWING * normal_vector(1);
  //    first_point.pose.position.z = ball_pose.pose.position.z - OFFSET_ALTITUDE_CLOSE;
  ////    points_list.push_back(first_point);


  //#ifdef ADJUST_ALTTITUDE
  //if(gimbal_angles_flag){
  //   // adjustAltitudeWithPerception(first_point);
  //}
  //#endif

  //#ifdef DONKEY_STATEGY
  //    donkeyStrategySaturation( first_point, 3);
  //#endif

  //  // Check distance
  //  safeDistanceWithNetSaturation(first_point);

  //  points_list.push_back(first_point);

  //  double actual_distance = distanceBetweenPoses(ball_pose,base_pose);
  //  inflation_radius = 2;
  //  gear_speed = calculateGearSpeed(previuos_distance_drone_close, actual_distance);
  //  previuos_distance_ball = actual_distance;

  //  return true;
  //    }else{
  //        return false;
  //    }

  std::cout << "FOLLOW BALL enteredddddddddd!!" << std::endl;

}

int BehaviorExecution::behaviorCatchBall(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed)
{

//  double CLOSE_FINGERS_AND_PRAY = 1.0;

//  geometry_msgs::PoseStamped ball_pose_HOLDER;

//#ifdef CATCH_BALL_CLOSE_NO_KALMAN
//  ball_pose_HOLDER = ball_close_pose_NO_kalman;
//#else
//  ball_pose_HOLDER = ball_pose;

//#endif


//  if (time_ball != behaviorCatchBallClose_lastTime){

//    if (distanceBetweenPoses(ball_pose_HOLDER,base_pose) < 2.0){

//      behaviorCatchBallClose_lastTime = time_ball;
//      //    Eigen::Vector3d normal_vector;
//      //    double module = distanceBetweenPoses(ball_pose_HOLDER,base_pose);

//      //    normal_vector(0) = (ball_pose_HOLDER.pose.position.x - base_pose.pose.position.x)/module;
//      //    normal_vector(1) = (ball_pose_HOLDER.pose.position.y - base_pose.pose.position.y)/module;
//      //    normal_vector(2) = (ball_pose_HOLDER.pose.position.z - base_pose.pose.position.z)/module;

//      points_list.clear();

//      //    double DISTANCE_BEHIND_BALL = distance_behind_ball_catch_DR;
//      //    double OFFSET_BALL_CATCH_ = offset_ball_catch_DR;


//      geometry_msgs::PoseStamped first_point;
//      first_point.pose.position.x = ball_pose_HOLDER.pose.position.x;
//      first_point.pose.position.y = ball_pose_HOLDER.pose.position.y;
//      first_point.pose.position.z = ball_pose_HOLDER.pose.position.z - gripperOffset(2) + OFFSET_BALL_CATCH_ + dynamic_offset_catch_z;

//      safeDistanceWithNetSaturation(first_point);
//      points_list.push_back(first_point);

//      geometry_msgs::PoseStamped second_point;
//      const double OFFSET_SECOND_POINT_Z = 1.0;
//      second_point.pose.position.x = ball_pose_HOLDER.pose.position.x + DISTANCE_BEHIND_BALL * normal_vector(0);
//      second_point.pose.position.y = ball_pose_HOLDER.pose.position.y + DISTANCE_BEHIND_BALL * normal_vector(1);
//      if (normal_vector(2) < 0){
//        second_point.pose.position.z = ball_pose_HOLDER.pose.position.z - gripperOffset(2) + OFFSET_BALL_CATCH_ + dynamic_offset_catch_z;
//      }
//      else{
//        second_point.pose.position.z = ball_pose_HOLDER.pose.position.z - gripperOffset(2) + OFFSET_BALL_CATCH_ + DISTANCE_BEHIND_BALL * normal_vector(2) + dynamic_offset_catch_z;
//      }
//      safeDistanceWithNetSaturation(second_point);

//      points_list.push_back(second_point);

//      inflation_radius = -1;
//      gear_speed = gear_speed_catch_ball_DR;
//      std::cout << "(distanceBetweenPoses(ball_pose_HOLDER,base_pose) : " << distanceBetweenPoses(ball_pose_HOLDER,base_pose) << std::endl;

//      if (distanceBetweenPoses(ball_pose_HOLDER,base_pose) < CLOSE_FINGERS_AND_PRAY) {
//        std::cout << "Catching ball and closing gripper "<< std::endl;

//#ifdef SEARCH_VARIABLE_ALTITUDE
//        stop_increasing_altitude_search = true;
//#endif
//        return 2;
//      }else{
//        return 1;
//      }
//    }
//  }
//  else {

//    return 3;
//  }

}


//Calculates distance between two poses
double BehaviorExecution::distanceBetweenPoses(geometry_msgs::PoseStamped pose_1, geometry_msgs::PoseStamped pose_2){
  return sqrt(pow(pose_1.pose.position.x - pose_2.pose.position.x,2)+
              pow(pose_1.pose.position.y - pose_2.pose.position.y,2)+
              pow(pose_1.pose.position.z - pose_2.pose.position.z,2));
}

double BehaviorExecution::distanceBetweenPosesXY(geometry_msgs::PoseStamped pose_1, geometry_msgs::PoseStamped pose_2){
  return sqrt(pow(pose_1.pose.position.x - pose_2.pose.position.x,2)+
              pow(pose_1.pose.position.y - pose_2.pose.position.y,2));
}

bool BehaviorExecution::behaviorTakeOff(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed){

  if(!save_pose_take_off_flag){
    save_pose_take_off_flag = true;
    this->take_off_pose_.pose.position.x = base_pose.pose.position.x;
    this->take_off_pose_.pose.position.y = base_pose.pose.position.y;
    this->take_off_pose_.pose.position.z = MIN_ALTITUDE;

    std::cout << "Position take off: " << base_pose.pose.position.x << ", " << base_pose.pose.position.y << ", "<< base_pose.pose.position.z << std::endl;
    points_list.clear();

    geometry_msgs::PoseStamped first_point;
    first_point.pose.position.x = base_pose.pose.position.x;
    first_point.pose.position.y = base_pose.pose.position.y;
    first_point.pose.position.z = base_pose.pose.position.z + MIN_ALTITUDE;
    points_list.push_back(first_point);

    inflation_radius = -1.0;
    gear_speed = gear_speed_takeoff_and_land_DR;
    return true;

  }else{
    return false;
  }



  // 2º Go to the initial altitude
}

bool BehaviorExecution::behaviorLand(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed){
  double THRESHOLD_LANDING = 2;

  geometry_msgs::PoseStamped landing_pose;
  landing_pose.pose.position.x = 28.9;
  landing_pose.pose.position.y = 48.65;
  landing_pose.pose.position.z = MIN_ALTITUDE;

  if(distanceBetweenPoses(base_pose,landing_pose) <  THRESHOLD_LANDING){
    std::cout << "Landing DJI " << std::endl;
    return true;
  }else{
    std::cout << "Position take off: " << landing_pose.pose.position.x << ", " << landing_pose.pose.position.y << ", "<< landing_pose.pose.position.z << std::endl;
    std::cout << "Position take of is : " << distanceBetweenPoses(base_pose,landing_pose) << " meters far. Threshold is : " << THRESHOLD_LANDING << std::endl;

    points_list.clear();
    geometry_msgs::PoseStamped first_point;
    first_point.pose.position.x = landing_pose.pose.position.x;
    first_point.pose.position.y = landing_pose.pose.position.y;
    first_point.pose.position.z = MIN_ALTITUDE;
    points_list.push_back(first_point);

    inflation_radius = -1.0;
    gear_speed = gear_speed_takeoff_and_land_DR;

    return false;
  }


}


//Publish yaw
void BehaviorExecution::publishExternalYaw(Item::State state, nav_msgs::Odometry & external_yaw ){
  double norm_x;
  double norm_y;



  //    switch(state){
  //        case Item::State::SEARCH:
  //#ifdef X_AXIS_SEARCH
  norm_x =  0;
  norm_y =  1; // Positive right and negative left

  //#else
  //            norm_x = (center_search_behavior.pose.position.x + center_search_behaviour_offset_yaw.pose.position.x) - base_pose.pose.position.x ;
  //            norm_y = (center_search_behavior.pose.position.y + center_search_behaviour_offset_yaw.pose.position.y) - base_pose.pose.position.y ;
  //#endif
  //        break;
  //        case Item::State::FOLLOW_DRONE_CLOSE:
  //            norm_x = base_pose.pose.position.x - drone_close_pose.pose.position.x;
  //            norm_y = base_pose.pose.position.y - drone_close_pose.pose.position.y;

  //        break;
  //        case Item::State::FOLLOW_DRONE_FAR:
  //            norm_x = base_pose.pose.position.x - drone_far_pose.pose.position.x;
  //            norm_y = base_pose.pose.position.y - drone_far_pose.pose.position.y;

  //        break;
  //        case Item::State::FOLLOW_BALL:
  //            norm_x = base_pose.pose.position.x - ball_pose.pose.position.x;
  //            norm_y = base_pose.pose.position.y - ball_pose.pose.position.y;

  //        break;
  //        case Item::State::FOCUS:
  //            norm_x = 0;//base_pose.pose.position.x - 20;
  //            norm_y = 1;//base_pose.pose.position.y - 50;

  //        break;
  //        case Item::State::CATCH_BALL:
  //#ifdef CATCH_BALL_CLOSE_NO_KALMAN
  //            norm_x = base_pose.pose.position.x - ball_close_pose_NO_kalman.pose.position.x;
  //            norm_y = base_pose.pose.position.y - ball_close_pose_NO_kalman.pose.position.y;
  //#else
  //            norm_x = base_pose.pose.position.x - ball_pose.pose.position.x;
  //            norm_y = base_pose.pose.position.y - ball_pose.pose.position.y;
  //#endif
  //        break;

  //    }
  double yaw_angle = atan2(norm_y,norm_x) + M_PI;
  //std::cout << "yaw_angle: " << yaw_angle*180/M_PI << std::endl;

  //nav_msgs::Odometry external_yaw;
  external_yaw.pose.pose.orientation.z = yaw_angle;
  last_external_yaw = yaw_angle;

  //external_yaw_publisher.publish(external_yaw);
}

void BehaviorExecution::donkeyStrategySaturation(geometry_msgs::PoseStamped & point, int mode){

  // Mode 1 drone close follow
  // Mode 3 drone far follow
  // Mode 2 ball follow

  double X_MIN =  height_arena_donkey_strategy/2 - height_box_donkey_strategy/2;
  double X_MAX =  height_arena_donkey_strategy/2 + height_box_donkey_strategy/2;
  double Y_MIN =  width_arena_donkey_strategy/2 - width_box_donkey_strategy/2;
  double Y_MAX =  width_arena_donkey_strategy/2 + width_box_donkey_strategy/2;

  bool COUT_DONKEY_STRATEGY = true;

  //X axis
  if(point.pose.position.x > X_MAX){
    point.pose.position.x = X_MAX;
    if(COUT_DONKEY_STRATEGY){
      std::cout <<" * * * * X MAX REACH --- DONKEY STRATEGY   * * * * "<< point.pose.position.x << std::endl;
      std::cout <<" * * * * X_MAX THRESHOLD : "<< X_MAX << std::endl;
    }
  }else if (point.pose.position.x < X_MIN) {
    point.pose.position.x = X_MIN;
    if(COUT_DONKEY_STRATEGY){
      std::cout <<" * * * * X MIN REACH --- DONKEY STRATEGY   * * * * "<< point.pose.position.x <<std::endl;
      std::cout <<" * * * * x_MIN THRESHOLD : "<< X_MIN << std::endl;
    }
  }
  //Y axis
  if(point.pose.position.y > Y_MAX){
    point.pose.position.y = Y_MAX;
    if(COUT_DONKEY_STRATEGY){
      std::cout <<" * * * * Y MAX --- DONKEY STRATEGY   * * * * "<< point.pose.position.y <<std::endl;
      std::cout <<" * * * * Y_MAX THRESHOLD : "<< Y_MAX << std::endl;
    }
  }else if (point.pose.position.y < Y_MIN) {
    point.pose.position.y = Y_MIN;
    if(COUT_DONKEY_STRATEGY){
      std::cout <<" * * * * Y MIN --- DONKEY STRATEGY   * * * * "<< point.pose.position.y <<std::endl;
      std::cout <<" * * * * Y_MIN THRESHOLD : "<< Y_MIN << std::endl;
    }
  }

#ifdef FRONT_DONKEY_STATEGY

  if(point.pose.position.y == Y_MAX || point.pose.position.y == Y_MIN ){
    if(mode == 1){
      if (drone_close_pose.pose.position.x > X_MAX){
        point.pose.position.x = X_MAX;
      }
      else if (drone_close_pose.pose.position.x < X_MIN){
        point.pose.position.x = X_MIN;
      }
      else{
        point.pose.position.x = drone_close_pose.pose.position.x;
      }
    } else if (mode == 2) {
      if (drone_far_pose.pose.position.x > X_MAX){
        point.pose.position.x = X_MAX;
      }
      else if (drone_far_pose.pose.position.x < X_MIN){
        point.pose.position.x = X_MIN;
      }
      else{
        point.pose.position.x = drone_far_pose.pose.position.x;
      }
    }else if (mode == 3) {
      if (ball_pose.pose.position.x > X_MAX){
        point.pose.position.x = X_MAX;
      }
      else if (ball_pose.pose.position.x < X_MIN){
        point.pose.position.x = X_MIN;
      }
      else{
        point.pose.position.x = ball_pose.pose.position.x;
      }

    }
    std::cout <<" * * * * FRONT DONKEY STRATEGY  * * * "<< std::endl;

  }



#endif

}

void BehaviorExecution::safeDistanceWithNetSaturation(geometry_msgs::PoseStamped & point){

  double SECURE_DISTANCE = 8;

  double X_MIN =  0 + SECURE_DISTANCE;
  double X_MAX =  height_arena_donkey_strategy - SECURE_DISTANCE;
  double Y_MIN =  0 + SECURE_DISTANCE;
  double Y_MAX =  width_arena_donkey_strategy - SECURE_DISTANCE;
  double Z_MAX = 17;
  double Z_MIN = 5;


  bool COUT_SECURE_DISTANCE = false;

  //X axis
  if(point.pose.position.x > X_MAX){
    point.pose.position.x = X_MAX;
    if(COUT_SECURE_DISTANCE){
      std::cout <<" * * * * X MAX REACH --- NET SECURE DISTANCE   * * * * "<< point.pose.position.x << std::endl;
      std::cout <<" * * * * X_MAX THRESHOLD : "<< X_MAX << std::endl;
    }
  }else if (point.pose.position.x < X_MIN) {
    point.pose.position.x = X_MIN;
    if(COUT_SECURE_DISTANCE){
      std::cout <<" * * * * X MIN REACH --- NET SECURE DISTANCE   * * * * "<< point.pose.position.x <<std::endl;
      std::cout <<" * * * * x_MIN THRESHOLD : "<< X_MIN << std::endl;
    }
  }
  //Y axis
  if(point.pose.position.y > Y_MAX){
    point.pose.position.y = Y_MAX;
    if(COUT_SECURE_DISTANCE){
      std::cout <<" * * * * Y MAX --- NET SECURE DISTANCE   * * * * "<< point.pose.position.y <<std::endl;
      std::cout <<" * * * * Y_MAX THRESHOLD : "<< Y_MAX << std::endl;
    }
  }else if (point.pose.position.y < Y_MIN) {
    point.pose.position.y = Y_MIN;
    if(COUT_SECURE_DISTANCE){
      std::cout <<" * * * * Y MIN --- NET SECURE DISTANCE   * * * * "<< point.pose.position.y <<std::endl;
      std::cout <<" * * * * Y_MIN THRESHOLD : "<< Y_MIN << std::endl;
    }
  }

  //Y axis
  if(point.pose.position.z > Z_MAX){
    point.pose.position.z = Z_MAX;
    if(COUT_SECURE_DISTANCE){
      std::cout <<" * * * * Z MAX --- NET SECURE DISTANCE   * * * * "<< point.pose.position.y <<std::endl;
      std::cout <<" * * * * Z_MAX THRESHOLD : "<< Z_MAX << std::endl;
    }
  }else if (point.pose.position.y < Z_MIN) {
    point.pose.position.y = Z_MIN;
    if(COUT_SECURE_DISTANCE){
      std::cout <<" * * * * Z MIN --- NET SECURE DISTANCE   * * * * "<< point.pose.position.y <<std::endl;
      std::cout <<" * * * * Z_MIN THRESHOLD : "<< Z_MIN << std::endl;
    }
  }
}




bool BehaviorExecution::getTrajectorySearch(std::vector<geometry_msgs::PoseStamped> & points_list){
  //create trayectory in the center

  double NUMBER_OF_POINTS= 20;

  std::cout << "Altitude of search: " << ALTITUDE_SEARCH << std::endl;

#ifdef X_AXIS_SEARCH
  double OFFSET_FROM_MIDDLE_Y = -5;
  double AMPLITUDE_MOVEMENT_X = 10;

  double OFFSET_X = (height_arena_donkey_strategy - AMPLITUDE_MOVEMENT_X)/2;

  const double SEARCH_OFFSET_ALTITUDE = 0.5;
  const double MAX_ALTITUDE = 13.0;

  points_list.clear();
  for (int i = 0; i < NUMBER_OF_POINTS + 1; ++i) {
    geometry_msgs::PoseStamped point;
    point.pose.position.x = AMPLITUDE_MOVEMENT_X/NUMBER_OF_POINTS * i + OFFSET_X;
    point.pose.position.y = width_arena_donkey_strategy/2 - OFFSET_FROM_MIDDLE_Y;
#ifdef SEARCH_VARIABLE_ALTITUDE
    point.pose.position.z = ALTITUDE_SEARCH + (double) counter_increasing_altitude_search * SEARCH_OFFSET_ALTITUDE;
    if (point.pose.position.z > MAX_ALTITUDE){
      point.pose.position.z = MAX_ALTITUDE;
    }
#else	
    point.pose.position.z = ALTITUDE_SEARCH;
#endif
    points_list.push_back(point);
  }

  if(distanceBetweenPoses(points_list.front(),base_pose) > distanceBetweenPoses(points_list.back(),base_pose)){

    points_list.clear();
    for (int i = 0; i<NUMBER_OF_POINTS+1; ++i) {
      geometry_msgs::PoseStamped point;
      point.pose.position.x = height_arena_donkey_strategy - (AMPLITUDE_MOVEMENT_X/NUMBER_OF_POINTS * i + OFFSET_X);
      point.pose.position.y = width_arena_donkey_strategy/2 - OFFSET_FROM_MIDDLE_Y;
#ifdef SEARCH_VARIABLE_ALTITUDE
      point.pose.position.z = ALTITUDE_SEARCH + (double) counter_increasing_altitude_search * SEARCH_OFFSET_ALTITUDE;
      if (point.pose.position.z > MAX_ALTITUDE){
        point.pose.position.z = MAX_ALTITUDE;
      }
#else
      point.pose.position.z = ALTITUDE_SEARCH;
#endif

      points_list.push_back(point);
    }
  }

#else
  double RANGE = M_PI;
  double AMPLITUDE_X = 5;
  double AMPLITUDE_Y = 10;
  double SIGN = -1.0;


  points_list.clear();
  for (int i = 0; i<NUMBER_OF_POINTS+1; ++i) {
    geometry_msgs::PoseStamped point;
    point.pose.position.x = center_search_behavior.pose.position.x + AMPLITUDE_X*fabs(cos(-RANGE/2 + RANGE/NUMBER_OF_POINTS * i));
    point.pose.position.y = center_search_behavior.pose.position.y + SIGN*AMPLITUDE_Y*sin(-RANGE/2 + RANGE/NUMBER_OF_POINTS * i);
    point.pose.position.z = ALTITUDE_SEARCH;
    points_list.push_back(point);
  }

  if(distanceBetweenPoses(points_list.front(),base_pose) > distanceBetweenPoses(points_list.back(),base_pose)){
    SIGN = 1.0;

    points_list.clear();
    for (int i = 0; i<NUMBER_OF_POINTS+1; ++i) {
      geometry_msgs::PoseStamped point;
      point.pose.position.x = center_search_behavior.pose.position.x + AMPLITUDE_X*fabs(cos(-RANGE/2 + RANGE/NUMBER_OF_POINTS * i));
      point.pose.position.y = center_search_behavior.pose.position.y + SIGN*AMPLITUDE_Y*sin(-RANGE/2 + RANGE/NUMBER_OF_POINTS * i);
      point.pose.position.z = ALTITUDE_SEARCH;
      points_list.push_back(point);
    }
    search_trajectory_clockwise_flag = true;
  }else{

    search_trajectory_clockwise_flag = false;
  }
#endif

}

void BehaviorExecution::adjustAltitudeWithPerception(geometry_msgs::PoseStamped & point){

  //    double MAX_ALTITUDE = 12;
  //    double target_angle = TARGET_ANGLE * M_PI / 180.0;
  //    double gimbal_angle = gimbal_angles_[0] * M_PI / 180.0;

  //    double distance_x = std::fabs(drone_close_pose.pose.position.x - base_pose.pose.position.x);
  //    point.pose.position.z +=  distance_x * (std::tan(gimbal_angle) - std::tan(target_angle));

  //    // Debug
  //    std::cout << "Distance being decreased: " << distance_x * (std::tan(gimbal_angle) - std::tan(target_angle)) << std::endl;
  //    std::cout << "Target angle: " << TARGET_ANGLE << "\t Current angle: " << gimbal_angles_[0] << std::endl;

  //    if(point.pose.position.z < MIN_ALTITUDE){
  //        std::cout << "Min altitude reached. Value" << point.pose.position.z << ". Threshold: "<< MIN_ALTITUDE   << std::endl;
  //        point.pose.position.z = MIN_ALTITUDE;

  //    }else if(point.pose.position.z > MAX_ALTITUDE){
  //        //std::cout << "Max altitude reached. Value" << point.pose.position.z << ". Threshold: "<< MAX_ALTITUDE   << std::endl;
  //        point.pose.position.z = MAX_ALTITUDE;
  //    }

  double MAX_ALTITUDE = 12;
  double target_angle = TARGET_ANGLE * M_PI / 180.0;
  double gimbal_angle = gimbal_angles_[0] * M_PI / 180.0;
  double GAIN = close_follow_distance_Z_ball_DR;

  point.pose.position.z =  base_pose.pose.position.z + GAIN * (gimbal_angle - target_angle);

  // Debug
  //std::cout << "Altitude: " << point.pose.position.z << std::endl;
  //std::cout << "Target angle: " << TARGET_ANGLE << "\t Current angle: " << gimbal_angles_[0] << std::endl;

  if(point.pose.position.z < MIN_ALTITUDE){
    std::cout << "Min altitude reached. Value" << point.pose.position.z << ". Threshold: "<< MIN_ALTITUDE   << std::endl;
    point.pose.position.z = MIN_ALTITUDE;

  }else if(point.pose.position.z > MAX_ALTITUDE){
    std::cout << "Max altitude reached. Value" << point.pose.position.z << ". Threshold: "<< MAX_ALTITUDE   << std::endl;
    point.pose.position.z = MAX_ALTITUDE;
  }
}

double BehaviorExecution::calculateGearSpeed(double previous_distance, double actual_distance){

  double H1;double H2; double D1; double D2;
  double THRESHOLD_MAX_DISTANCE =  10.0;
  //std::cout << "actual_distance :" << actual_distance << std::endl;


  if (actual_distance < previous_distance && actual_distance < THRESHOLD_MAX_DISTANCE){
    H1 = h1_slow_DR; H2= h2_slow_DR; D1 = d1_slow_DR; D2 = d2_slow_DR;
    //std::cout << "calculateGearSpeed SLOW " << std::endl;

  }else{
    H1 = h1_fast_DR; H2= h2_fast_DR; D1 = d1_fast_DR; D2 = d2_fast_DR;
    //std::cout << "calculateGearSpeed FAST " << std::endl;

  }

  double gear_speed  =(H1+H2)/2 +   ((H2-H1) /2) *tanh( (4 * (actual_distance - (D1+D2)/2) / (D2-D1)) ) ;
  //std::cout << "gear_speed " << gear_speed << std::endl;

  return gear_speed;
}



void BehaviorExecution::resetFocusFlags(){
  focus_descending_flag = false;
  focus_achieved_goal_altitude = false;
  focus_trajectory_in_progress_flag = false;
  end_focus_state = false;
}

void BehaviorExecution::activateBallDetectionAllTime(bool activate_ball_detector_all_time){

  ACTIVATE_BALL_DETECTOR_ALL_TIME = activate_ball_detector_all_time;
}

void BehaviorExecution::setCatchCounter(int counter_catch){

#ifdef DYNAMIC_OFFSET_CATCH
  double OFFSET_INTERVAL = 0.05;
  double OFFSET_CATCH_MAX = 0.5;
  dynamic_offset_catch_z = (double) counter_catch * OFFSET_INTERVAL;

  if(dynamic_offset_catch_z > OFFSET_CATCH_MAX){
    dynamic_offset_catch_z = OFFSET_CATCH_MAX;
  }

  //std::cout << "dynamic_offset_catch_z is increased to: " << dynamic_offset_catch_z << std::endl;
#endif

}
