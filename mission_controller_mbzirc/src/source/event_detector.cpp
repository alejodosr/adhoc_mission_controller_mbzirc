#include "event_detector.h"

//Constructor
EventDetector::EventDetector(){
	last_event = Item::Event::UNKNOWN;
    counter_target_far = 0;
    counter_target_close = 0;
    counter_ball_close = 0;
    motion_direction_flag = false;
    time_target_drone_far = 0;
    time_target_drone_close = 0;
    time_ball = 0;
    gripperOffset(0) = 0;
    gripperOffset(1) = 0;
    gripperOffset(2) = 0;
    ball_catch_flag = false;
    arrived_to_search_flag = false;
    ACTIVATE_BALL_DETECTOR_ALL_TIME = false;
    counter_catch = 0;

    wait_key_take_off = false;
}

//Destructor
EventDetector::~EventDetector() {}


//Set poses
void EventDetector::setPoses(geometry_msgs::PoseStamped ball_pose,
        geometry_msgs::PoseStamped base_pose,geometry_msgs::PoseStamped drone_far_pose, geometry_msgs::PoseStamped drone_close_pose )
{
	this->ball_pose = ball_pose;
	this->base_pose = base_pose;
        this->drone_far_pose = drone_far_pose;
        this->drone_close_pose = drone_close_pose;
}



//Set last time poses were checked
//void EventDetector::setTime(double time_target_drone_far,double time_target_drone_close,double time_ball){
//	this->time_target_drone_far = time_target_drone_far;
//	this->time_target_drone_close = time_target_drone_close;
//	this->time_ball = time_ball;
//}

//Set gripperOffset
void EventDetector::setGripperOffset(Eigen::Vector3d gripperOffset){
	this->gripperOffset = gripperOffset;
}

//Set counter consecutive target detections with far detector
void EventDetector::setCounterFarTargetDetector(int counter){
    this->counter_target_far = counter;
}

//Set counter consecutive target detections with close detector
void EventDetector::setCounterCloseTargetDetector(int counter){
    this->counter_target_close = counter;
}

////Set motion_direction_flag
//void EventDetector::setMotionDirectionFlag(bool value){
//    this->motion_direction_flag = value;
//}

//Set ball_catch_flag 
void EventDetector::setBallCatchFlag(bool value){
    this->ball_catch_flag = value;
}

//Set counter consecutive ball detections with close detector
void EventDetector::setCounterCloseBallDetector(int counter){
    this->counter_ball_close = counter;
}

//Set counter consecutive ball detections with close detector
//void EventDetector::setTimeElapsedRecoverThreshold(int counter){
//    this->counter_ball_close = counter;
//}
void EventDetector::setArrivedToSearchFlag(int flag){
    this->arrived_to_search_flag = flag;
}

void EventDetector::setBallTime(double time_ball){
        this->time_ball = time_ball;
}



//Generate event based on TF and current state
bool EventDetector::generateEvent(Item::State current_state, Item::Event & next_event){
    // return true whem there is an event
    switch(current_state){
        std::cout << " EVENT GENERATOR"<< std::endl;
        // *********** State::START_STATE ****************
        case Item::State::START_STATE:
            if(wait_key_take_off){
                        next_event = Item::Event::TAKE_OFF;
                        return true;
            }else {
                        return false;
        }
        break;
        // *********** State::TAKE_OFF ****************
        case Item::State::TAKE_OFF:
            double TOLERANCE_ERROR;
            TOLERANCE_ERROR = 1;
            std::cout << " TAKE_OFF in progress. Height is: "<< base_pose.pose.position.z  <<" and min height is "<< 5.0 << std::endl;
            if(this->base_pose.pose.position.z + TOLERANCE_ERROR > /*min_height_take_off*/5.0){
                        next_event = Item::Event::TAKE_OFF_COMPLETED;
                        return true;
            }else{
                        return false;
            }
        break;
        // *********** State::SEARCHING ****************
        case Item::State::SEARCH:

        // añadir evento de ball_catch_flag y eliminar el resto ed eventos
        //  Item::Event::SUCCESSFUL_CATCH
        // Check we send false when we dont need transicition

        //std::cout << " SEARCH" << std::endl;
//            if (counter_target_close >= n_min_drone_close_detections){
//                next_event = Item::Event::CLOSE_DRONE_DETECTED;
//            }else if(counter_target_far >= n_min_drone_far_detections){
//                next_event = Item::Event::FAR_DRONE_DETECTED;
//            }else if(counter_ball_close >= 1){
//                next_event = Item::Event::BALL_DETECTED;
          if (ball_catch_flag){
            next_event = Item::Event::SUCCESSFUL_CATCH;
          }else{
            return false;
          }
          break;
        // *********** State::FOLLOW_DRONE_FAR ****************
        case Item::State::FOLLOW_DRONE_FAR:
        //std::cout << " FAR " << std::endl;

            if(counter_target_close >= n_min_drone_close_detections){
                next_event = Item::Event::CLOSE_DRONE_DETECTED;
            }else if (counter_target_far < n_min_drone_far_detections){
                next_event = Item::Event::DRONE_DETECTION_LOST;
            }else if(counter_ball_close >= 1){
                next_event = Item::Event::BALL_DETECTED;
            }else{
                return false;
            }
        break;
        // *********** State::FOLLOW_DRONE_CLOSE ****************
        case Item::State::FOLLOW_DRONE_CLOSE:
            if(motion_direction_flag){
                next_event = Item::Event::FOLLOW_COMPLETED;
            }else if (counter_target_close < n_min_drone_close_detections){
                startTimerRecover();
                next_event = Item::Event::DRONE_DETECTION_LOST;
            }else if(counter_ball_close >= 1){
                next_event = Item::Event::BALL_DETECTED;
            }else{
                return false;
            }
        break;
        // *********** State::FOCUS ***************
        case Item::State::FOCUS:

            //if (counter_target_close >= n_min_drone_close_detections){
            //    next_event = Item::Event::CLOSE_DRONE_DETECTED;
            //    std::cout << "***CLOSE_DRONE_DETECTED****" << std::endl;
            //}else if(counter_target_far >= n_min_drone_far_detections_focus){
            //    next_event = Item::Event::FAR_DRONE_DETECTED;
            //    std::cout << "**FAR_DRONE_DETECTED*****" << std::endl;
            //}else 
	    if(arrived_to_search_flag){
                next_event = Item::Event::ARRIVED_TO_SEARCH;
                std::cout << "***ARRIVED_TO_SEARCH****" << std::endl;
                arrived_to_search_flag = false;
            //}else if(counter_ball_close >= 1){
            //    next_event = Item::Event::BALL_DETECTED;
            }else{
                return false;
            }
        break;
        // ********** State::RECOVER *****************
        case Item::State::RECOVER:
            //if (counter_target_close >= n_min_drone_close_detections){
            //    next_event = Item::Event::CLOSE_DRONE_DETECTED;
            //}else if(counter_target_far >= n_min_drone_far_detections){
            //    next_event = Item::Event::FAR_DRONE_DETECTED;
            //}else 
	    if(checkTimeOutRecover()){
                next_event = Item::Event::TIMEOUT_RECOVER;
            //}else if(counter_ball_close >= 1){
            //    next_event = Item::Event::BALL_DETECTED;
            }else{
                return false;
            }
        break;
//        case Item::State::INTERCEPTION:
//            if(counter_ball_close >= n_min_ball_close_detections){
//                next_event = Item::Event::CLOSE_BALL_DETECTED;
//            }else{
//                next_event = Item::Event::BALL_DETECTION_LOST;
//            }
//        break;

        case Item::State::FOLLOW_BALL:
                std::cout << "distanceBetweenPosesXY (base_pose,ball_pose) : "<< distanceBetweenPosesXY(base_pose,ball_pose) << std::endl;
                std::cout << "distanceBetweenPosesZ(base_pose,ball_pose) : "<< distanceBetweenPosesZ(base_pose,ball_pose) << std::endl;
                //checkTimeOutBallCatch();
            if((distanceBetweenPosesXY(base_pose,ball_pose) < distance_threshold_catch_DR) && (distanceBetweenPosesZ(base_pose,ball_pose) < 2.0) && checkTimeOutBallCatch()){
                startTimerBallCatchInCatch();
		next_event = Item::Event::EXECUTE_CATCH;
                counter_catch++;
		//return false;
            }else if (counter_ball_close < 1) {
                next_event = Item::Event::BALL_DETECTION_LOST;
                startTimerRecover();
            }else{
                return false;
            }

        break;

        // ********** State::CATCH_BALL *****************
        case Item::State::CATCH_BALL: // DUDA counter_ball_close < n_min_ball_close_detections
            if (ball_catch_flag && counter_ball_close < n_min_ball_close_detections){
                next_event = Item::Event::SUCCESSFUL_CATCH;                
            }else if (!ball_catch_flag && counter_ball_close < 1){
                next_event = Item::Event::FAILED_CATCH;
            }
	    else if(checkTimeOutBallCatchInCatch()){
	    	next_event = Item::Event::FAILED_CATCH;
	    }
	    else{
                return false;
            }

        break;
        // ********** Item::State::LAND *****************
        case Item::State::LAND:
            //next_event = Item::Event::TAKE_OFF;
            return false;
        break;
        default:
            return false;
        break;
    }
    last_event = next_event;
    printEvent(next_event);
    return true;
}

void EventDetector::printEvent(Item::Event event){
    switch(event){
        case Item::Event::TAKE_OFF:
                        std::cout << "\033[1;94m[Event] TAKE_OFF\033[0m" << std::endl;
        break;
        case Item::Event::TAKE_OFF_COMPLETED:
			std::cout << "\033[1;94m[Event] TAKE_OFF_COMPLETED\033[0m" << std::endl;
        break;
        case Item::Event::FAR_DRONE_DETECTED:
			std::cout << "\033[1;94m[Event] FAR_DRONE_DETECTED\033[0m" << std::endl;
        break;
        case Item::Event::CLOSE_DRONE_DETECTED:
            std::cout << "\033[1;94m[Event] CLOSE_DRONE_DETECTED\033[0m" << std::endl;
        break;
        case Item::Event::FOLLOW_COMPLETED:
            std::cout << "\033[1;94m[Event] FOLLOW_COMPLETED\033[0m" << std::endl;
        break;
        case Item::Event::DRONE_DETECTION_LOST:
            std::cout << "\033[1;94m[Event] DRONE_DETECTION_LOST\033[0m" << std::endl;
        break;
        case Item::Event::BALL_DETECTED:
            std::cout << "\033[1;94m[Event] BALL_DETECTED\033[0m" << std::endl;
        break;
        case Item::Event::BALL_DETECTION_LOST:
            std::cout << "\033[1;94m[Event] BALL_DETECTION_LOST\033[0m" << std::endl;
        break;
        case Item::Event::SUCCESSFUL_CATCH:
            std::cout << "\033[1;94m[Event] SUCCESSFUL_CATCH\033[0m" << std::endl;
        break;
        case Item::Event::FAILED_CATCH:
            std::cout << "\033[1;94m[Event] FAILED_CATCH\033[0m" << std::endl;
        break;
        case Item::Event::TIMEOUT_RECOVER:
            std::cout << "\033[1;94m[Event] TIMEOUT_RECOVER\033[0m" << std::endl;
        break;
        case Item::Event::ARRIVED_TO_SEARCH:
            std::cout << "\033[1;94m[Event] ARRIVED_TO_SEARCH\033[0m" << std::endl;
        break;
    default:
        std::cout << "Unkown event" << std::endl;

        break;
    }
}

void EventDetector::startTimerRecover(){
    initial_time_recover = ros::Time::now().toSec();
}

void EventDetector::setTimeRecoverThreshold(int threshold){
    time_elapsed_recover_threshold = threshold;

}

bool EventDetector::checkTimeOutRecover(){
      double elapsed_time = ros::Time::now().toSec()-  initial_time_recover;
      std::cout << "elapsed_time recover:"<< elapsed_time << std::endl;
      std::cout << "time_elapsed_recover_threshold:"<< time_elapsed_recover_threshold << std::endl;

      return elapsed_time > time_elapsed_recover_threshold;
}

void EventDetector::startTimerBallCatchInCatch(){
    initial_time_ball_catch = ros::Time::now().toSec();
}


bool EventDetector::checkTimeOutBallCatchInCatch(){
      double elapsed_time = ros::Time::now().toSec()-  initial_time_ball_catch;
      double time_elapsed_ball_catch_threshold = 8.0;
      std::cout << "elapsed_time ball catch:"<< elapsed_time << std::endl;
      std::cout << "time_elapsed_ball_catch_threshold:"<< time_elapsed_ball_catch_threshold << std::endl;

      return elapsed_time > time_elapsed_ball_catch_threshold;
}




void EventDetector::activateBallDetectionAllTime(bool activate_ball_detector_all_time){

    ACTIVATE_BALL_DETECTOR_ALL_TIME = activate_ball_detector_all_time;
}

double EventDetector::distanceBetweenPosesXY(geometry_msgs::PoseStamped pose_1, geometry_msgs::PoseStamped pose_2){
    return sqrt(pow(pose_1.pose.position.x - pose_2.pose.position.x,2)+
        pow(pose_1.pose.position.y - pose_2.pose.position.y,2));
}

double EventDetector::distanceBetweenPosesZ(geometry_msgs::PoseStamped pose_1, geometry_msgs::PoseStamped pose_2){
    return std::fabs(pose_1.pose.position.z - pose_2.pose.position.z);
}

bool EventDetector::checkTimeOutBallCatch(){
    double THRESHOLD_TIME_OUT_CATCH = 1.0;

    //std::cout << "fabs(ros::Time::now().toSec() - time_ball): *******************************" << fabs(ros::Time::now().toSec() - time_ball) << std::endl;

    if ( fabs(ros::Time::now().toSec() - time_ball) > THRESHOLD_TIME_OUT_CATCH){
        return false;
    }else{
        return true;
    }
 }

void EventDetector::getCatchCounter(int & counter_catch){
    counter_catch = this->counter_catch;

}


