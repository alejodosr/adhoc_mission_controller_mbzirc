#include <stdio.h>
#include <iostream>
#include <chrono>
#include <iomanip>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include "ros/ros.h"
#include "std_srvs/Empty.h"
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include "tf_conversions/tf_eigen.h"

void tfPublisher(float x, float y, float z, std::string parent, std::string child);

int main(int argc,char **argv) {
	ros::init(argc, argv, "mission_controller_tester");
	ros::NodeHandle n;
	
    int count;

    ros::Rate loop_rate(1);
    try
    {    
        for(count = 0; ros::ok() && count < 10;count++)
        {  
          ROS_INFO("Sending tfs");

          tfPublisher(count,2,1,"odom","base_link");

          tfPublisher(20,2,1,"odom","drone_far");

          tfPublisher(20,2,1,"odom","drone_far_kalman");
          //tfPublisher(1,3,2,"odom","drone_close");
          //tfPublisher(1,3,1,"odom","ball_close_kalman");
          //tfPublisher(5,5,1,"odom","ball_close");
          tfPublisher(0,0,1,"base_link_stabilized","gripper");

          ros::spinOnce();
          ros::Duration(3).sleep();
        }
        for(count = 0; ros::ok() && count < 10;count++)
        {  
          ROS_INFO("Sending tfs");

          tfPublisher(count+10,2,1,"odom","base_link");
          tfPublisher(20,2,1,"odom","drone_close_kalman");
          tfPublisher(20,2,1,"odom","drone_close");
          tfPublisher(20,2,1,"odom","ball_close_kalman");
          tfPublisher(20,2,1,"odom","ball_close");
          tfPublisher(5,5,1,"base_link_stabilized","gripper");

          ros::spinOnce();
          ros::Duration(3).sleep();
          //loop_rate.sleep();
        }
    }
    catch (std::exception &ex)
    {
        std::cout << "[ROSNODE] Exception :" << ex.what() << std::endl;
    }
    return 0;
}


void tfPublisher(float x, float y, float z, std::string parent, std::string child){
  static tf::TransformBroadcaster br;
  tf::Transform transform;
  transform.setOrigin(tf::Vector3(x, y, z) );
  tf::Quaternion q;
  q.setRPY(0, 0, 0);
  transform.setRotation(q);
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), parent, child));	
}