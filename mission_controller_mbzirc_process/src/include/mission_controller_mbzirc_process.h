#ifndef MISSION_CONTROLLER_MBZIRC_PROCESS_H
#define MISSION_CONTROLLER_MBZIRC_PROCESS_H

#include <string>
#include <robot_process.h>
#include "ros/ros.h"
#include <math.h>
#include "eigen3/Eigen/Eigen"
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Byte.h>

//#include "state_machine.h"
#include "event_detector.h"
#include "behavior_execution.h"

#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_listener.h>
#include "tf_conversions/tf_eigen.h"
//nav_msgs
#include <nav_msgs/Odometry.h>

//geometry
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Vector3Stamped.h>

#include <mission_controller_mbzirc_process/SetPoints.h>
#include <mission_controller_mbzirc_process/CheckCatch.h>

#include <droneMsgsROS/droneCommand.h>

#include <boost/algorithm/string/erase.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

#include <dynamic_reconfigure/server.h>
#include <mission_controller_mbzirc_process/MyParamsConfig.h>


#define UseVisualizer

class MissionControllerProcess : public RobotProcess
{
public:
    MissionControllerProcess();
    ~MissionControllerProcess();

    double get_moduleRate();
    void dynamicTunningCallback(mission_controller_mbzirc::MyParamsConfig &config, uint32_t level);

private: /*RobotProcess*/
    void ownSetUp();
    void ownStart();
    void ownStop();
    void ownRun();
    double rate;


    std::string drone_id_namespace;

    BehaviorExecution behavior_execution;
    EventDetector event_detector;
    StateMachine state_machine;

    Item::State current_state;
    Item::Event event;
    bool successfull_execution;

    tf::TransformListener listener;

    //Topics
    std::string ball_odometry_topic;
    std::string drone_odometry_topic;
    std::string planner_setpoints_topic;
    std::string motion_controller_mode_topic;
    std::string detector_mode_topic;
    std::string gripper_odometry_topic;
    std::string external_yaw_topic;
    std::string ball_posetopic;
    std::string base_posetopic;
    std::string gimbal_angle_topic;
    std::string high_level_command_topic;
    std::string gripper_topic;
    
    //Timeouts
    double start_state_timeout;
    double takeoff_state_timeout;
    double searching_state_timeout;
    double followfar_state_timeout;
    double followclose_state_timeout;
    double recover_state_timeout;
    double interception_state_timeout;
    double catchball_state_timeout;
    double land_state_timeout;


    //Min number of detections
    int n_min_drone_close_detections;
    int n_min_drone_far_detections;
    int n_min_ball_close_detections;

    //Subs and pubs
    ros::Subscriber ball_pose_subscriber;
    ros::Subscriber detector_mode_subscriber;
    ros::Subscriber motion_controller_mode_subscriber;
    ros::Subscriber gimbal_angle_subscriber;

    //ros::Publisher gripper_pose_publisher;
    //ros::Publisher target_drone_dometry_publisher;
    ros::Publisher external_yaw_publisher;
    ros::Publisher planner_publisher;
    ros::Publisher high_level_command_publisher;
    ros::Publisher gripper_state_publisher;

    //Visualizer
    ros::Publisher target_drone_far_publisher;
    ros::Publisher target_drone_close_publisher;
    ros::Publisher counter_target_far_publisher;
    ros::Publisher counter_target_close_publisher;
    ros::Publisher counter_ball_publisher;
    ros::Publisher current_state_publisher;
    ros::Publisher mpc_points_publisher;
    ros::Publisher last_event_publisher;
    ros::Subscriber command_subscriber;

    //Service
    ros::ServiceClient catch_ball_client;


    //Variables
    geometry_msgs::PoseStamped ball_pose;
    geometry_msgs::PoseStamped ball_pose_NO_kalman;

    geometry_msgs::PoseStamped base_link_pose;
    geometry_msgs::PoseStamped drone_far_pose;
    geometry_msgs::PoseStamped drone_close_pose;
    geometry_msgs::PoseStamped future_drone_pose;

    mission_controller_mbzirc_process::SetPoints planner_points;
    std::vector<geometry_msgs::PoseStamped> points_list;
    float inflation_radius;
    float gear_speed;
    nav_msgs::Odometry external_yaw;

    //Variables for adjust altitude with image
    double min_altitude_external_altitude_control;
    double target_angle_external_altitude_control;
    geometry_msgs::PoseStamped center_search_behaviour;
    geometry_msgs::PoseStamped center_search_behaviour_offset_yaw;

    double width_box_donkey_strategy;
    double height_box_donkey_strategy;
    double width_arena_donkey_strategy;
    double height_arena_donkey_strategy;
    double altitude_search;

    double y_quad;
    double offset_predictive;
    double offset_predictive_width;
    double gripper_offset_adjust;
    double offset_x_search;
    int tent_strategy;

    Eigen::Vector3d gripperOffset;

    double time_target_drone_far;
    double time_target_drone_close;
    double time_ball;

    bool ball_catched;
    bool gripper_closed;

    double time_threshold_drone_close;
    double time_threshold_drone_far;
    double time_threshold_ball;
    double time_threshold_recover;

    double initial_time_recover;

    int counter_target_far;
    int counter_target_close;
    int counter_ball_close;

    bool far_counter_enable_flag;
    bool close_counter_enable_flag;
    bool ball_counter_enable_flag;

    double drone_close_time;
    double ball_close_time;

    bool last_drone_close_kalman;
    bool last_ball_close_kalman;
    bool last_ball_close;

    void getVariablesFromTF();
    bool getGripperOffsetFromTF(Eigen::Vector3d & transformDroneToGripper);
    bool getPoseFromTF(std::string frame,geometry_msgs::PoseStamped & pose);
    bool getTimeFromTF(std::string frame, double & time);
    bool getSequenceFromTF(std::string frame, double & last_time, int & counter);
    void transformVectorStringToDouble(std::string & input, geometry_msgs::PoseStamped & output);

    void checkTimeOutCounterTF(std::string frame, int & counter, double time_threshold);
    void detectorModeCallback(const std_msgs::Int32::ConstPtr &msg);
    void gimbalAngleCallback(const geometry_msgs::Vector3Stamped &msg);
    void commandsCallback(const std_msgs::Int32::ConstPtr &msg);
    void takeOffPublisher();
    void landPublisher();
    void publishPoints(std::vector<geometry_msgs::PoseStamped> points_list, float inflation_radius, float  gear_speed);
    void publishExternalYaw(nav_msgs::Odometry external_yaw, Item::State state);
    void enableCloseCounter();
    void initializeTimerRecover();
    void setElapsedTimeReover();

    void disableFarCounter();
    void enableFarCounter();
    void disableCloseCounter();

    void closeGripper();
    void openGripper();
    bool checkBallInGripper();

    bool checkPoseWithinQuadrant(geometry_msgs::PoseStamped &temp);

};

#endif
