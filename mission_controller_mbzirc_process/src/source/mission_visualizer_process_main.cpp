#include "mission_visualizer_process_main.h"

//Callback Functions
void targetFarDronePoseCallback(const geometry_msgs::PoseStamped::ConstPtr &msg) {
    target_far_pose = *msg; 
}

void targetCloseDronePoseCallback(const geometry_msgs::PoseStamped::ConstPtr &msg) {
    target_close_pose = *msg; 
}

void counterTargetFarCallback(const std_msgs::Int32::ConstPtr &msg){
    if (msg->data != counter_far_consecutive_detections){    
        counter_far_consecutive_detections= msg->data;
        counter_far_time = ros::Time::now().toSec();
    }
}
void counterTargetCloseCallback(const std_msgs::Int32::ConstPtr &msg){
    if (msg->data != counter_close_consecutive_detections){    
        counter_close_consecutive_detections = msg->data;
        counter_close_time = ros::Time::now().toSec();
    }
}
void counterBallCallback(const std_msgs::Int32::ConstPtr &msg){
    if (msg->data != counter_ball_consecutive_detections){    
        counter_ball_consecutive_detections = msg->data;
        counter_ball_time = ros::Time::now().toSec();
    }
}

void eventCallback(const std_msgs::Int32::ConstPtr &msg){
    if (msg->data != last_event){
        last_event8 = last_event7;
        last_event7 = last_event6;
        last_event6 = last_event5;
        last_event5 = last_event4;
        last_event4 = last_event3;
        last_event3 = last_event2;
        last_event2 = last_event1;
        last_event1 = last_event;
        last_event = msg->data;
    }
}
void stateCallback(const std_msgs::Int32::ConstPtr &msg){
    if (msg->data != current_state){
        past_state9 = past_state8;
        past_state8 = past_state7;
        past_state7 = past_state6;
        past_state6 = past_state5;
        past_state5 = past_state4;
        past_state4 = past_state3;
        past_state3 = past_state2;
        past_state2 = past_state1;
        past_state1 = current_state;
        current_state = msg->data;        
    }
}

void batteryCallback(const droneMsgsROS::battery::ConstPtr& msg){ 
    battery_msg=*msg;
}

void odometryCallback(const nav_msgs::Odometry::ConstPtr &msg){
    base_drone_pose = *msg;
    tf2::Matrix3x3 m(tf2::Quaternion 
        (base_drone_pose.pose.pose.orientation.x,
            base_drone_pose.pose.pose.orientation.y,
            base_drone_pose.pose.pose.orientation.z,
            base_drone_pose.pose.pose.orientation.w));
    
    m.getRPY(roll, pitch, yaw);
    if (std::isnan(yaw)) yaw = 0.0;
}

int main(int argc, char **argv)
{
    //OUTPUT FORMAT
    interface_printout_stream << std::fixed << std::setprecision(2) << std::setfill('0'); //<< std::showpos

    //ROS 
    ros::init(argc, argv, "mission_visualizer_process");
    ros::NodeHandle n;

    //Configuration
    ros::param::get("~robot_namespace", drone_id_namespace);
    ros::param::get("~frequency", frequency);
    ros::param::get("~mission_time_seconds", mission_time_seconds);

    //Subscribers
    target_far_pose_sub =  n.subscribe<geometry_msgs::PoseStamped>("/"+drone_id_namespace+"/mission_visualizer/target_far_drone", 1, &targetFarDronePoseCallback);
    target_close_pose_sub =  n.subscribe<geometry_msgs::PoseStamped>("/"+drone_id_namespace+"/mission_visualizer/target_close_drone", 1, &targetCloseDronePoseCallback);
    counter_target_far_sub =  n.subscribe<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/counter_far", 1, &counterTargetFarCallback);
    counter_target_close_sub =  n.subscribe<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/counter_close", 1, &counterTargetCloseCallback);
    counter_ball_sub =  n.subscribe<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/counter_ball", 1, &counterBallCallback);
    current_state_sub =  n.subscribe<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/state", 10, &stateCallback);
    last_event_sub =  n.subscribe<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/event", 10, &eventCallback);
    drone_pose_sub =  n.subscribe<nav_msgs::Odometry>("/msf_core/odometry", 1, &odometryCallback);
    battery_sub=n.subscribe("/"+drone_id_namespace+"/battery", 1, &batteryCallback);
    //Publishers
    command_pub = n.advertise<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/command", 1);

    //Initialization
    counter_far_consecutive_detections = 0;counter_far_time = 0;
    counter_close_consecutive_detections = 0;counter_close_time = 0;
    counter_ball_consecutive_detections = 0;counter_ball_time = 0;
    last_event2 = -1;last_event1 = -1;past_state3 = -1;past_state2 = -1;past_state1 = -1;
    past_state4 = -1;past_state5 = -1;past_state6 = -1;past_state7 = -1;past_state8 = -1; past_state9 = -1;
    last_event3 = -1;last_event4 = -1;last_event5 = -1;last_event6 = -1;last_event7 = -1; last_event8 = -1;
    roll = 0; pitch = 0; yaw = 0;
    mission_started = false;
    //ncurses initialization (output text)
    initscr();
    start_color();
    use_default_colors();  
    curs_set(0);
    noecho();
    nodelay(stdscr, TRUE);
    erase();
    refresh();
    init_pair(1, COLOR_GREEN, -1);
    init_pair(2, COLOR_RED, -1);
    init_pair(3, COLOR_CYAN, -1);
    init_pair(4, COLOR_YELLOW, -1);

    //Input variable
    char command = 0;
    int command_confirm = -1;
    
    //Rate
    ros::Rate loop_rate(frequency);

    mission_time = ros::Time::now().toSec();

    //Loop
    while (ros::ok()) {
        
        //Read messages
        ros::spinOnce();

        printStaticMenu();

        //Time remaining and battery
        move(2,72);
        if(mission_started){
            int aux = mission_time_seconds - (ros::Time::now().toSec() - mission_time);
            if (aux <= 0){    
                attron(COLOR_PAIR(2));printTime(0);attroff(COLOR_PAIR(2)); 
            }else{
                printTime(aux);                   
            }
        }else printTime(mission_time_seconds);

        //Battery
        move(3,73);
        printBattery();

        //States
        move(3,5);
        attron(COLOR_PAIR(1));printState(current_state);attroff(COLOR_PAIR(1));
        move(4,5);
        printState(past_state1);
        move(5,5);
        printState(past_state2);
        move(6,5);
        printState(past_state3);
        move(7,5);
        printState(past_state4);
        move(8,5);
        printState(past_state5);
        move(9,5);
        printState(past_state6);
        move(10,5);
        printState(past_state7);
        move(11,5);
        printState(past_state8);
        move(12,5);
        printState(past_state9);

        //Events
        move(3,34);
        printw("-");
        move(4,30);
        attron(COLOR_PAIR(3));printEvent(last_event);attroff(COLOR_PAIR(3));
        move(5,30);
        printEvent(last_event1);
        move(6,30);
        printEvent(last_event2);
        move(7,30);
        printEvent(last_event3);
        move(8,30);
        printEvent(last_event4);
        move(9,30);
        printEvent(last_event5);
        move(10,30);
        printEvent(last_event6);
        move(11,30);
        printEvent(last_event7);
        move(12,30);
        printEvent(last_event8);

        //Drone pose
        move(15,9);
        printStream(base_drone_pose.pose.pose.position.x);printw(" m  ");
        move(16,9);
        printStream(base_drone_pose.pose.pose.position.y);printw(" m  ");
        move(17,9);
        printStream(base_drone_pose.pose.pose.position.z);printw(" m  ");

        //Drone speed
        move(15,34);
        printStream(base_drone_pose.twist.twist.linear.x);printw(" m/s  ");
        move(16,34);
        printStream(base_drone_pose.twist.twist.linear.y);printw(" m/s  ");
        move(17,34);
        printStream(base_drone_pose.twist.twist.linear.z);printw(" m/s  ");

        //Drone orientation
        move(15,64);
        printStream(roll);printw(" rad  ");  
        move(16,64);
        printStream(pitch);printw(" rad  ");
        move(17,64);
        printStream(yaw);printw(" rad  ");

        //Far drone pose
        move(20,49);
        printStream(target_far_pose.pose.position.x);
        move(21,49);
        printStream(target_far_pose.pose.position.y);
        move(22,49);
        printStream(target_far_pose.pose.position.z);
        move(23,49);
        printStream(calculateDistance(target_far_pose));printw(" m  ");  

        //Close drone pose
        move(20,69);
        printStream(target_close_pose.pose.position.x);
        move(21,69);
        printStream(target_close_pose.pose.position.y);
        move(22,69);
        printStream(target_close_pose.pose.position.z);
        move(23,69);
        printStream(calculateDistance(target_close_pose));printw(" m  ");   

        //Counter consecutive detections
        move(21,13);
        printStreamInt(counter_far_consecutive_detections);
        move(22,13);
        printStreamInt(counter_close_consecutive_detections);
        move(23,13);
        printStreamInt(counter_ball_consecutive_detections);

        //Last time seen detections    
        move(21,27);
        if (counter_far_time == 0) printTime(0);
        else printTime(ros::Time::now().toSec() - counter_far_time);
        move(22,27);
        if (counter_close_time == 0) printTime(0);
        else printTime(ros::Time::now().toSec() - counter_close_time);
        move(23,27);
        if (counter_ball_time == 0) printTime(0);
        else printTime(ros::Time::now().toSec() - counter_ball_time);

        //  Read input
        command = getch();
        switch (command){
            case '0':  // STOP & RESET
                if (command_confirm == -1){
                    command_aux2 = 1;
                    printCommand(command_aux2);
                    command_confirm = 0;
                }else{
                    if(command_confirm == 0){
                        command_aux2=2;
                        printCommand(command_aux2);
                        // Execute STOP & RESET
                        mission_started = false;
                        msg_command.data = 0;  
                        command_pub.publish(msg_command);     
                    }else{
                        command_aux2=3;
                        printCommand(command_aux2);                 
                    }
                    command_confirm = -1;
                }
            break;
            case '1':  // MANUAL TAKE_OFF
                if (command_confirm == -1){
                    command_aux2 = 4;
                    printCommand(command_aux2);
                    command_confirm = 1;
                }else{
                    if(command_confirm == 1){
                        command_aux2 = 5;
                        printCommand(command_aux2);
                        // Execute TAKE_OFF
                        if(!mission_started){
                            mission_started = true;
                            mission_time = ros::Time::now().toSec();
                        }
                        msg_command.data = 1;  
                        command_pub.publish(msg_command);
                    }else{
                        command_aux2=3;
                        printCommand(command_aux2);               
                    }
                    command_confirm = -1;
                }
            break;
            case '2':  // MANUAL LAND
                if (command_confirm == -1){
                    command_aux2 = 6;
                    printCommand(command_aux2);
                    command_confirm = 2;
                }else{
                    if(command_confirm == 2){
                        command_aux2 = 7;
                        printCommand(command_aux2);
                        // Execute LAND  
                        msg_command.data = 2;  
                        command_pub.publish(msg_command);     
                    }else{
                        command_aux2=3;        
                        printCommand(command_aux2);        
                    }
                    command_confirm = -1;
                }
            break;
            case '3':  // MANUAL NEXT_STATE
                if (command_confirm == -1){
                    command_aux2 = 8;
                    command_confirm = 3;
                    printCommand(command_aux2);
                }else{
                    if(command_confirm == 3){
                        command_aux2= 9;
                        printCommand(command_aux2);
                        // Execute NEXT_STATE 
                        msg_command.data = 3;  
                        command_pub.publish(msg_command); 
                    }else{
                        command_aux2=3; 
                        printCommand(command_aux2);              
                    }
                    command_confirm = -1;
                }
            break;
            case '4':  // MANUAL PREV_STATE
                if (command_confirm == -1){
                    command_aux2 = 10;
                    command_confirm = 4;
                    printCommand(command_aux2);
                }else{
                    if(command_confirm == 4){
                        command_aux2 = 11;
                        printCommand(command_aux2);
                        // Execute PREVIOUS STATE
                        msg_command.data = 4;  
                        command_pub.publish(msg_command);         
                    }else{
                        command_aux2=3;   
                        printCommand(command_aux2);              
                    }
                    command_confirm = -1;
                }
            break;
            case ' ':  // [SPACE] CLEAN AND REPRINT SCREEN
                endwin();
                refresh();
                clear();
            break;
        }
        refresh();
        loop_rate.sleep();
    }

    //End ncurses
    endwin();
    return 0;
}

//Print float using stringstream
void printStream(float var) {
    interface_printout_stream.clear();
    interface_printout_stream.str(std::string());
    if (var > -0.01){
        interface_printout_stream << std::setw(5) << std::internal << fabs(var);
        attron(COLOR_PAIR(1));printw(" %s",interface_printout_stream.str().c_str());attroff(COLOR_PAIR(1));
    }else{
        interface_printout_stream << std::setw(6) << std::internal << var;
        attron(COLOR_PAIR(2));printw("%s",interface_printout_stream.str().c_str());attroff(COLOR_PAIR(2));
    }
} 

//Print float using stringstream
void printStreamInt(int var) {
    interface_printout_stream.clear();
    interface_printout_stream.str(std::string());
    interface_printout_stream << std::setw(2) << std::internal << var;
    printw(" %s",interface_printout_stream.str().c_str());
} 

//Print float using stringstream
void printDistance(float var) {
    interface_printout_stream.clear();
    interface_printout_stream.str(std::string());
    interface_printout_stream << std::setw(5) << std::internal << var;
    printw("%s",interface_printout_stream.str().c_str());
} 

//Print double using stringstream
void printStream(double var) {
    interface_printout_stream.clear();
    interface_printout_stream.str(std::string());
    if (var > -0.01){
        interface_printout_stream << std::setw(5) << std::internal << fabs(var);
        attron(COLOR_PAIR(1));printw(" %s",interface_printout_stream.str().c_str());attroff(COLOR_PAIR(1));
    }else{
        interface_printout_stream << std::setw(6) << std::internal << var;
        attron(COLOR_PAIR(2));printw("%s",interface_printout_stream.str().c_str());attroff(COLOR_PAIR(2));
    }
}

//Print state
void printState(int state) {
    switch(state){
        case (int)Item::State::START_STATE:
            printw("%s","START_STATE         ");
        break;
        case (int)Item::State::TAKE_OFF:
            printw("%s","TAKE_OFF            ");
        break;
        case (int)Item::State::SEARCH:
            printw("%s","SEARCHING           ");
        break;
        case (int)Item::State::FOLLOW_DRONE_CLOSE:
            printw("%s","FOLLOW_DRONE_CLOSE  ");
        break;
        case (int)Item::State::FOLLOW_DRONE_FAR:
            printw("%s","FOLLOW_DRONE_FAR    ");
        break;
        case (int)Item::State::RECOVER:
            printw("%s","RECOVER             ");
        break;
        case (int)Item::State::INTERCEPTION:
            printw("%s","INTERCEPTION        ");
        break;
        case (int)Item::State::CATCH_BALL:
            printw("%s","CATCH_BALL          ");
        break;
        case (int)Item::State::FOLLOW_BALL:
            printw("%s","FOLLOW_BALL          ");
        break;
        case (int)Item::State::LAND:
            printw("%s","LAND                ");
        break;
        case (int)Item::State::FOCUS:
            printw("%s","FOCUS                ");
        break;
        default:
            printw("%s","    -               ");        
        break;
    }
}

//Print event
void printEvent(int event) {
    switch(event){
        case (int)Item::Event::TAKE_OFF:
            printw("%s","TAKE_OFF              ");
        break;
        case (int)Item::Event::TAKE_OFF_COMPLETED:
            printw("%s","TAKE_OFF_COMPLETED    ");
        break;
        case (int)Item::Event::FAR_DRONE_DETECTED:
            printw("%s","FAR_DRONE_DETECTED    ");
        break;
        case (int)Item::Event::CLOSE_DRONE_DETECTED:
            printw("%s","CLOSE_DRONE_DETECTED  ");
        break;
        case (int)Item::Event::FOLLOW_COMPLETED:
            printw("%s","FOLLOW_COMPLETED      ");
        break;
        case (int)Item::Event::DRONE_DETECTION_LOST:
            printw("%s","DRONE_DETECTION_LOST  ");
        break;
        case (int)Item::Event::BALL_DETECTED:
            printw("%s","CLOSE_BALL_DETECTED   ");
        break;
        case (int)Item::Event::BALL_DETECTION_LOST:
            printw("%s","BALL_DETECTION_LOST   ");
        break;
        case (int)Item::Event::EXECUTE_CATCH:
            printw("%s","EXECUTE_CATCH         ");
        break;
        case (int)Item::Event::SUCCESSFUL_CATCH:
            printw("%s","SUCCESSFUL_CATCH      ");
        break;
        case (int)Item::Event::FAILED_CATCH:
            printw("%s","FAILED_CATCH          ");
        break;
        case (int)Item::Event::ARRIVED_TO_SEARCH:
            printw("%s","ARRIVED_TO_SEARCH          ");
        break;
        case (int)Item::Event::TIMEOUT_RECOVER:
            printw("%s","TIMEOUT_RECOVER          ");
        break;
        default:
            printw("%s","    -               ");
        break;
    }
}

void printStaticMenu(){
    move(0,32);
    printw("- MISSION VIEWER -");

    move(2,5);
    printw("States ");
    move(2,30);
    printw("Events    ");

    move(2,55);
    printw("Time remaining: ");
    move(3,55);
    printw("Battery:    ");

    //Drone pose
    move(14,5);
    printw("DRONE POSE");
    move(15,5);
    printw("x: ");
    move(16,5);
    printw("y: ");
    move(17,5);
    printw("z: ");

    //Drone speed
    move(14,30);
    printw("DRONE SPEED");
    move(15,30);
    printw("x: ");
    move(16,30);
    printw("y: ");
    move(17,30);
    printw("z: ");

    //Drone orientation
    move(14,55);
    printw("DRONE ORIENTATION");
    move(15,55);
    printw("roll: ");
    move(16,55);
    printw("pitch: ");
    move(17,55);
    printw("yaw: ");

    //Far drone pose
    move(19,42);
    printw("FAR DRONE POSE");
    move(20,42);
    printw("x: ");
    move(21,42);
    printw("y: ");
    move(22,42);
    printw("z: ");
    move(23,42);
    printw("Dist: ");

    //Close drone pose
    move(19,62);
    printw("CLOSE DRONE POSE");
    move(20,62);
    printw("x: ");
    move(21,62);
    printw("y: ");
    move(22,62);
    printw("z: ");
    move(23,62);
    printw("Dist: ");

    //Counter consecutive detections
    move(20,3);
    printw("COUNTER DETECTIONS");
    move(21,3);
    printw("Far:   ");
    move(22,3);
    printw("Close: ");
    move(23,3);
    printw("Ball: ");

    //Time consecutive detections
    move(20,22);
    printw("| LAST TIME SEEN");

    move(25,30);
    printw("- MISSION COMMANDS -");
    move(27,6);
    printw("0:STOP & RESET  1:TAKE_OFF  2:LAND  3:NEXT_STATE  4:PREVIOUS_STATE");

    //Command line
    printCommand(command_aux2);
}

//Print battery charge
void printBattery(){
    interface_printout_stream << std::fixed << std::setprecision(0) << std::setfill(' ');
    interface_printout_stream.clear();
    interface_printout_stream.str(std::string());
    interface_printout_stream << std::setw(2) << std::internal << battery_msg.batteryPercent;
    if(battery_msg.batteryPercent == 100) {
        attron(COLOR_PAIR(1));printw(" %s",interface_printout_stream.str().c_str());attroff(COLOR_PAIR(1));
    }
    if(battery_msg.batteryPercent > 50 && battery_msg.batteryPercent < 100) {
        attron(COLOR_PAIR(1));printw(" %s",interface_printout_stream.str().c_str());attroff(COLOR_PAIR(1));
    }
    if(battery_msg.batteryPercent <= 50 && battery_msg.batteryPercent > 20) {
        attron(COLOR_PAIR(4));printw(" %s",interface_printout_stream.str().c_str());attroff(COLOR_PAIR(4));
    }
    if(battery_msg.batteryPercent <= 20) {
        attron(COLOR_PAIR(2));printw(" %s",interface_printout_stream.str().c_str());attroff(COLOR_PAIR(2));     
    }
    printw(" %%");
    interface_printout_stream << std::fixed << std::setprecision(2) << std::setfill('0');
}

//Print distance between target pose and base drone pose
float calculateDistance(geometry_msgs::PoseStamped target){
    return sqrt(pow(base_drone_pose.pose.pose.position.x-target.pose.position.x,2)
        +pow(base_drone_pose.pose.pose.position.y-target.pose.position.y,2)
        +pow(base_drone_pose.pose.pose.position.z-target.pose.position.z,2));
}

void printTime(int seconds){
    interface_printout_stream.clear();
    interface_printout_stream.str(std::string());
    interface_printout_stream << std::setfill('0') << std::setw(2) << (seconds / 60) << ":"
    << std::setfill('0') << std::setw(2) << (seconds % 60);
    printw(" %s",interface_printout_stream.str().c_str());
}

void printCommand(int command){
    switch(command){
        case 1:
            move(29,7); clrtoeol(); 
            attron(COLOR_PAIR(3));
            printw("   Press 0 to confirm 'STOP & RESET' or other key to cancel command  ");
            attroff(COLOR_PAIR(3));
        break;
        case 2:
            move(29,7);clrtoeol();
            attron(COLOR_PAIR(3));
            printw("                Last manual command: STOP & RESET                   ");
            attroff(COLOR_PAIR(3));
        break;
        case 3:
            move(29,7);clrtoeol(); 
            attron(COLOR_PAIR(2));
            printw("                          Command canceled                          ");   
            attroff(COLOR_PAIR(2));      
        break;
        case 4:
            move(29,7);clrtoeol(); 
            attron(COLOR_PAIR(3));
            printw("    Press 1 to confirm 'TAKE_OFF' or other key to cancel command     ");
            attroff(COLOR_PAIR(3));
        break;
        case 5:
            move(29,7);clrtoeol(); 
            attron(COLOR_PAIR(3));
            printw("                    Last manual command: TAKE_OFF                   ");
            attroff(COLOR_PAIR(3));
        break;
        case 6:
            move(29,7);clrtoeol(); 
            attron(COLOR_PAIR(3));
            printw("      Press 2 to confirm 'LAND' or other key to cancel command       ");
            attroff(COLOR_PAIR(3));
        break;
        case 7:
            move(29,7);clrtoeol(); 
            attron(COLOR_PAIR(3));
            printw("                      Last manual command: LAND                     ");
            attroff(COLOR_PAIR(3));
        break;
        case 8:
            move(29,7);clrtoeol();
            attron(COLOR_PAIR(3));
            printw("    Press 3 to confirm 'NEXT_STATE' or other key to cancel command   ");
            attroff(COLOR_PAIR(3));
        break;
        case 9:
            move(29,7);clrtoeol();
            attron(COLOR_PAIR(3));
            printw("                   Last manual command: NEXT_STATE                  ");
            attroff(COLOR_PAIR(3));
        break;
        case 10:
            move(29,7);clrtoeol();
            attron(COLOR_PAIR(3));
            printw("  Press 4 to confirm 'PREVIOUS_STATE' or other key to cancel command ");
            attroff(COLOR_PAIR(3));
        break;
        case 11:
            move(29,7);clrtoeol();
            attron(COLOR_PAIR(3));
            printw("                Last manual command: PREVIOUS_STATE                 ");
            attroff(COLOR_PAIR(3));
        break;
    }
}