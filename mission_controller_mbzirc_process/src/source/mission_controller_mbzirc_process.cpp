#include "mission_controller_mbzirc_process.h"

MissionControllerProcess::MissionControllerProcess(){  
  state_machine = StateMachine();
  behavior_execution = BehaviorExecution();
  event_detector = EventDetector();
}

MissionControllerProcess::~MissionControllerProcess(){}

double MissionControllerProcess::get_moduleRate(){ 
  return rate;
}

void MissionControllerProcess::ownSetUp()
{   // Configs
  ros::param::get("~frequency", rate);
  ros::param::get("~robot_namespace", drone_id_namespace);
  ros::param::get("~time_threshold_drone_close", time_threshold_drone_close);
  ros::param::get("~time_threshold_drone_far", time_threshold_drone_far);
  ros::param::get("~time_threshold_ball", time_threshold_ball);
  ros::param::get("~time_threshold_recover", time_threshold_recover);

  //Topics
  ros::param::get("~ball_posetopic", ball_posetopic);
  ros::param::get("~base_posetopic", base_posetopic);
  ros::param::get("~planner_setpoints_topic", planner_setpoints_topic);
  ros::param::get("~motion_controller_mode_topic", motion_controller_mode_topic);
  ros::param::get("~detector_mode_topic", detector_mode_topic);
  ros::param::get("~gripper_odometry_topic", gripper_odometry_topic);
  ros::param::get("~external_yaw_topic", external_yaw_topic);
  ros::param::get("~gimbal_angle_topic", gimbal_angle_topic);
  ros::param::get("~high_level_command_topic", high_level_command_topic);
  ros::param::get("~gripper_topic", gripper_topic);


  //States timeouts

  //External altitude control variables
  ros::param::get("~min_altitude_external_altitude_control", min_altitude_external_altitude_control);
  ros::param::get("~target_angle_external_altitude_control", target_angle_external_altitude_control);

  // Center search behaviour
  std::string tmp;

  //Get vector from .launch
  ros::param::get("~center_search_behaviour", tmp);
  transformVectorStringToDouble(tmp, center_search_behaviour);

  ros::param::get("~center_search_behaviour_offset_yaw", tmp);
  transformVectorStringToDouble(tmp, center_search_behaviour_offset_yaw);

  //Donkey strategy parameters

  ros::param::get("~width_box_donkey_strategy", width_box_donkey_strategy);
  ros::param::get("~height_box_donkey_strategy", height_box_donkey_strategy);

  ros::param::get("~width_arena_donkey_strategy", width_arena_donkey_strategy);
  ros::param::get("~height_arena_donkey_strategy", height_arena_donkey_strategy);
  ros::param::get("~altitude_search", altitude_search);

  //Detector
  ros::param::get("~n_min_drone_close_detections", n_min_drone_close_detections);
  ros::param::get("~n_min_drone_far_detections", n_min_drone_far_detections);
  ros::param::get("~n_min_ball_close_detections", n_min_ball_close_detections);

  // Adhoc parameters
  ros::param::get("~y_quad", y_quad);
  ros::param::get("~offset_predictive", offset_predictive);
  ros::param::get("~offset_predictive_width", offset_predictive_width);
  ros::param::get("~gripper_offset_adjust", gripper_offset_adjust);
  ros::param::get("~offset_x_search", offset_x_search);
  ros::param::get("~tent_strategy", tent_strategy);

  behavior_execution.y_quad = y_quad;
  behavior_execution.offset_predictive = offset_predictive;
  behavior_execution.offset_predictive_width = offset_predictive_width;
  behavior_execution.gripper_offset_adjust = gripper_offset_adjust;
  behavior_execution.offset_x_search = offset_x_search;
  behavior_execution.tent_strategy = tent_strategy;

  std::cout << "ADHOC PARAMETERS" << std::endl;
  std::cout << "y_quad: " << y_quad << std::endl;
  std::cout << "offset_predictive: " << offset_predictive << std::endl;
  std::cout << "offset_predictive_width: " << offset_predictive_width << std::endl;
  std::cout << "gripper_offset_adjust: " << gripper_offset_adjust << std::endl;
  std::cout << "offset_x_search: " << offset_x_search << std::endl;
  std::cout << "tent_strategy: " << tent_strategy << std::endl;

  //Timeout recover
  ros::param::get("~recover_state_timeout", recover_state_timeout);

  event_detector.setTimeRecoverThreshold(recover_state_timeout);

  //Set min number of detections
  event_detector.n_min_drone_close_detections = n_min_drone_close_detections;
  event_detector.n_min_drone_far_detections = n_min_drone_far_detections;

  //TODO
  int RATIO_FOR_FOCUS_FAR = 3;
  event_detector.n_min_drone_far_detections_focus =  RATIO_FOR_FOCUS_FAR * n_min_drone_far_detections;

  event_detector.n_min_ball_close_detections = n_min_ball_close_detections;
  event_detector.min_height_take_off = min_altitude_external_altitude_control;

  //Set constants for external gimbal altitude
  behavior_execution.setExternalGimbalAltitude(target_angle_external_altitude_control, min_altitude_external_altitude_control);
  behavior_execution.center_search_behavior = center_search_behaviour;
  behavior_execution.center_search_behaviour_offset_yaw = center_search_behaviour_offset_yaw;
  behavior_execution.width_box_donkey_strategy = width_box_donkey_strategy;
  behavior_execution.height_box_donkey_strategy = height_box_donkey_strategy;
  behavior_execution.width_arena_donkey_strategy = width_arena_donkey_strategy;
  behavior_execution.height_arena_donkey_strategy = height_arena_donkey_strategy;
  behavior_execution.ALTITUDE_SEARCH = altitude_search;
}

void MissionControllerProcess::ownStart(){
  ros::NodeHandle n;
  std::cout << std::endl << std::endl << "MISSION CONTROLLER STATES SEQUENCE"<< std::endl;
  std::cout << "----------------------------------"<< std::endl;
  current_state = Item::State::START_STATE;
  state_machine.printState(current_state);
  //event = -1;
  successfull_execution = true;

  //time_threshold = 2;
  counter_target_far = 0;
  counter_target_close = 0;
  counter_ball_close = 0;
  //first_detection_flag = false;

  far_counter_enable_flag = false;
  close_counter_enable_flag = false;
  ball_counter_enable_flag = false;
  ball_catched = false;
  gripper_closed = false;

  bool ACTIVATE_BALL_DETECTION_ALL_TIME_FLAG = true;

  state_machine.activateBallDetectionAllTime(ACTIVATE_BALL_DETECTION_ALL_TIME_FLAG);
  event_detector.activateBallDetectionAllTime(ACTIVATE_BALL_DETECTION_ALL_TIME_FLAG);
  behavior_execution.activateBallDetectionAllTime(ACTIVATE_BALL_DETECTION_ALL_TIME_FLAG);

  //event_detector.setMotionDirectionFlag(false); //??????
  event_detector.setBallCatchFlag(false); //?????????

  //    ball_pose.pose.position.x = 0.0;
  //    ball_pose.pose.position.y = 0.0;
  //    ball_pose.pose.position.z = 2.0;

  //old_odometry_ball_x = 0.0;
  //old_odometry_ball_y = 0.0;
  //old_odometry_ball_z = 0.0;

  //previous_time_secs = ros::Time::now().toSec();
  time_target_drone_far = ros::Time::now().toSec();
  time_target_drone_close = ros::Time::now().toSec();
  time_ball = ros::Time::now().toSec();

  //following_ball = false;
  external_yaw_publisher =  n.advertise<nav_msgs::Odometry>(external_yaw_topic, 1);
  planner_publisher  =  n.advertise<mission_controller_mbzirc_process::SetPoints>(planner_setpoints_topic, 1);
  high_level_command_publisher =  n.advertise<droneMsgsROS::droneCommand>(high_level_command_topic, 1);
  mpc_points_publisher   =  n.advertise<geometry_msgs::PoseStamped>("command/pose",1);

  detector_mode_subscriber = n.subscribe<std_msgs::Int32>("/"+drone_id_namespace+"/detector_mode", 1, &MissionControllerProcess::detectorModeCallback, this);
  gimbal_angle_subscriber = n.subscribe(gimbal_angle_topic, 1, &MissionControllerProcess::gimbalAngleCallback, this);

  catch_ball_client = n.serviceClient<mission_controller_mbzirc_process::CheckCatch>("/check_catch");

  //Commands from visualizer
  command_subscriber = n.subscribe("mission_visualizer/command", 1, &MissionControllerProcess::commandsCallback, this);

  gripper_state_publisher =  n.advertise<std_msgs::Byte>(gripper_topic, 1);

  //Visualizer
#ifdef UseVisualizer
  target_drone_far_publisher =  n.advertise<geometry_msgs::PoseStamped>("/"+drone_id_namespace+"/mission_visualizer/target_far_drone", 1);
  target_drone_close_publisher =  n.advertise<geometry_msgs::PoseStamped>("/"+drone_id_namespace+"/mission_visualizer/target_close_drone", 1);
  counter_target_far_publisher =  n.advertise<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/counter_far", 1);
  counter_target_close_publisher =  n.advertise<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/counter_close", 1);
  counter_ball_publisher =  n.advertise<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/counter_ball", 1);
  current_state_publisher =  n.advertise<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/state", 1);
  last_event_publisher =  n.advertise<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/event", 1);
#endif

}

void MissionControllerProcess::ownStop(){   

}

void MissionControllerProcess::ownRun()
{

  //Get variables from TF
  getVariablesFromTF();

  //        std::cout <<"counter_target_far :"<< counter_target_far << std::endl;
  //        std::cout <<"counter_target_close :"<< counter_target_close << std::endl;
  //std::cout <<"far_counter_enable_flag :"<< far_counter_enable_flag << std::endl;
  //std::cout <<"close_counter_enable_flag :"<< close_counter_enable_flag << std::endl;


  //Generate event
  if (event_detector.generateEvent(current_state, event)){
    //Next state
    state_machine.nextState(current_state,event);

#ifdef UseVisualizer
    std_msgs::Int32 msg;
    msg.data = (int)current_state;
    current_state_publisher.publish(msg);
    msg.data = (int)event;
    last_event_publisher.publish(msg);
#endif

    //Check if next state is available before executing the current one
    while(true){
      if (event_detector.generateEvent(current_state, event)){
        std::cout << "--------------- PRINT STATE AND EVENT -------------------"<< std::endl;
        state_machine.printState(current_state);
        state_machine.nextState(current_state,event);

#ifdef UseVisualizer
        std_msgs::Int32 msg;
        msg.data = (int)current_state;
        current_state_publisher.publish(msg);
        msg.data = (int)event;
        last_event_publisher.publish(msg);
#endif

      }else break;
    }
  }

  //Returns:
  // 0 data does not need to be sent
  // 1 planner goal points need to be sent
  // 2 external yaw need to be sent
  // 3 planner goal and external yaw need to be sent
  // 4 take off high level command needs to be sent
  // 5 land high level command needs to be sent
  //Execute State
  //std::cout << behavior_execution.executeState(current_state,points_list,inflation_radius,gear_speed, external_yaw) << std::endl;

  switch(behavior_execution.executeState(current_state,points_list,inflation_radius,gear_speed, external_yaw)){
  case 0:
    //Nothing to do

    break;
  case 1:
    publishPoints(points_list,inflation_radius,gear_speed);

    break;
  case 2:
    publishExternalYaw(external_yaw, current_state);
    break;
  case 3:
    publishPoints(points_list,inflation_radius,gear_speed);
    publishExternalYaw(external_yaw, current_state);

    break;
  case 4:
    takeOffPublisher();
    publishPoints(points_list,inflation_radius,gear_speed);

    break;
  case 5:
    landPublisher();
    break;
  case 6:
    closeGripper();
    gripper_closed = true;
    publishPoints(points_list,inflation_radius,gear_speed);
    publishExternalYaw(external_yaw, current_state);
    break;
  }

  //Returns:
  // 0 do nothing
  // 1 enable far and close
  // 2 disable and reset far and close
  // 3 disable far and enable close
  // 4 start search behavior

  switch(behavior_execution.resetFlags(current_state, far_counter_enable_flag , close_counter_enable_flag, ball_counter_enable_flag)){
  case 0:
    //Nothing to do
    break;
  case 1:
    event_detector.setArrivedToSearchFlag(true);
    break;
  }

  if(!far_counter_enable_flag){
    counter_target_far = 0;

  }
  if(!close_counter_enable_flag){
    counter_target_close = 0;
  }
  if(!ball_counter_enable_flag){
    counter_ball_close = 0;
  }

//  if (current_state == Item::State::CATCH_BALL && fabs(ros::Time::now().toSec() - ball_close_time) > (time_threshold_ball - 3)){
//    if(checkBallInGripper()){
//      event_detector.ball_catch_flag = true;
//      ball_catched = true;
//    }
//  }
//  if(current_state != Item::State::CATCH_BALL && !ball_catched){
//    openGripper();
//  }

  if (current_state == Item::State::SEARCH && (fabs(ros::Time::now().toSec() - ball_close_time) > 3.0) && gripper_closed){
    if(checkBallInGripper()){
      event_detector.ball_catch_flag = true;
      ball_catched = true;
    }
    else{
      openGripper();
      gripper_closed = false;
    }
  }

  //    //Check timeout
  //    if(state_machine.checkTimeout(current_state)){
  //        current_state = Item::State::RECOVER;
  //    }

}

bool MissionControllerProcess::checkPoseWithinQuadrant(geometry_msgs::PoseStamped &temp){

  if (tent_strategy){
    if((temp.pose.position.x > 17.5) && (temp.pose.position.x < 32.0)){
      if(temp.pose.position.y < y_quad){
        if((temp.pose.position.z > 7.0) && (temp.pose.position.z < 16.0)){
          return true;
        }
        else if(temp.pose.position.z < 7.0){
          temp.pose.position.z = 7.0;
          return true;
        }
        else{
          temp.pose.position.z = 16.0;
          return true;
        }
      }
      else{
        return false;
      }
    }
    else{
      return false;
    }
  }
  else{
    if((temp.pose.position.x > 8.0) && (temp.pose.position.x < 22.5)){
      if(temp.pose.position.y < y_quad){
        if((temp.pose.position.z > 7.0) && (temp.pose.position.z < 16.0)){
          return true;
        }
        else if(temp.pose.position.z < 7.0){
          temp.pose.position.z = 7.0;
          return true;
        }
        else{
          temp.pose.position.z = 16.0;
          return true;
        }
      }
      else{
        return false;
      }
    }
    else{
      return false;
    }
  }


}

// Get ball, target drone and base drone poses and gripper offset from TF, return false if a variable couldn't be gotten
void MissionControllerProcess::getVariablesFromTF(){

  getPoseFromTF("/base_link",base_link_pose);
  behavior_execution.setBaseLinkPose(base_link_pose);
  //Get gripper offset
  getGripperOffsetFromTF(gripperOffset);
  behavior_execution.setGripperOffset(gripperOffset);

  getPoseFromTF("/drone_far",drone_far_pose);
  behavior_execution.setDroneFarPose(drone_far_pose);

  // temporary pose
  geometry_msgs::PoseStamped temp;

  getPoseFromTF("/drone_close_kalman",temp);
  last_drone_close_kalman = checkPoseWithinQuadrant(temp);
  if (last_drone_close_kalman){
    behavior_execution.drone_seen = true;
    drone_close_pose = temp;
    behavior_execution.setDroneClosePose(temp);

    if(getTimeFromTF("/drone_close", drone_close_time)){
      behavior_execution.setDroneCloseTime(drone_close_time);
    }
  }
//  else{
//    std::cout << "drone_close_kalman being discarded due to quadrant" << std::endl;
//  }

  getPoseFromTF("/ball_close_kalman",temp);
  last_ball_close_kalman = checkPoseWithinQuadrant(temp);
  if (last_ball_close_kalman){
    ball_pose = temp;
    behavior_execution.setBallPose(temp);
  }
//  else{
//    std::cout << "ball_close_kalman being discarded due to quadrant" << std::endl;
//  }


  getPoseFromTF("/ball_close",temp);
  last_ball_close = checkPoseWithinQuadrant(temp);
  if (last_ball_close){
    behavior_execution.ball_seen = true;
    ball_pose_NO_kalman = temp;
    behavior_execution.setBallClosePose_NO_kalman(temp);

    if(getTimeFromTF("/ball_close", ball_close_time)){
      behavior_execution.setBallTime(ball_close_time);
      event_detector.setBallTime(ball_close_time);
    }
  }
//  else{
//    std::cout << "ball_close being discarded due to quadrant" << std::endl;
//  }


  event_detector.setPoses(ball_pose, base_link_pose, drone_far_pose, drone_close_pose);




  getPoseFromTF("/future_drone_close_kalman_10", future_drone_pose);
  behavior_execution.setDroneFuturePose(future_drone_pose);



  //Update counters from detectors
  checkTimeOutCounterTF("/drone_far", counter_target_far, time_threshold_drone_far);
  event_detector.setCounterFarTargetDetector(counter_target_far);

  checkTimeOutCounterTF("/drone_close", counter_target_close, time_threshold_drone_close);
  event_detector.setCounterCloseTargetDetector(counter_target_close);

//  if(getTimeFromTF("/drone_close", drone_close_time)){
//    behavior_execution.setDroneCloseTime(drone_close_time);
//  }

  checkTimeOutCounterTF("/ball_close", counter_ball_close, time_threshold_ball);
  event_detector.setCounterCloseBallDetector(counter_ball_close);

//  if(getTimeFromTF("/ball_close", ball_close_time)){
//    behavior_execution.setBallTime(ball_close_time);
//    event_detector.setBallTime(ball_close_time);

//  }

  int counter_catch;
  event_detector.getCatchCounter(counter_catch);
  behavior_execution.setCatchCounter(counter_catch);

#ifdef UseVisualizer
  target_drone_far_publisher.publish(drone_far_pose);
  target_drone_close_publisher.publish(drone_close_pose);
  std_msgs::Int32 msgInt;
  msgInt.data = counter_target_far;
  counter_target_far_publisher.publish(msgInt);
  msgInt.data = counter_target_close;
  counter_target_close_publisher.publish(msgInt);
  msgInt.data = counter_ball_close;
  counter_ball_publisher.publish(msgInt);
#endif

  //return res;
}

//Get gripper offset
bool MissionControllerProcess::getGripperOffsetFromTF(Eigen::Vector3d & transformDroneToGripper){
  tf::StampedTransform transform;
  try{
    listener.lookupTransform("base_link_stabilized", "gripper", ros::Time(0), transform);
    double x = transform.getOrigin().x();
    double y = transform.getOrigin().y();
    double z = transform.getOrigin().z();

    transformDroneToGripper(0) = x;
    transformDroneToGripper(1) = y;
    transformDroneToGripper(2) = z;

    event_detector.setGripperOffset(transformDroneToGripper);

    return true;
  }catch (tf::TransformException ex){
    //ROS_ERROR("%s",ex.what());
    return false;
  }
}

//Get pose frame from tf with odom
bool MissionControllerProcess::getPoseFromTF(std::string frame,geometry_msgs::PoseStamped & pose){
  tf::StampedTransform transform;
  try{
    listener.lookupTransform("odom", frame, ros::Time(0), transform);

    double x = transform.getOrigin().x();
    double y = transform.getOrigin().y();
    double z = transform.getOrigin().z();

    if(!(std::isfinite(x) && std::isfinite(y) && std::isfinite(z))){
      //first_detection_flag = false;
      ROS_WARN(" * * * * TF VALUE IS NAN  * * * * ");
      return false;
    }else{
      pose.pose.position.x = x;
      pose.pose.position.y = y;
      pose.pose.position.z = z;
      return true;
    }
  }catch (tf::TransformException ex){
    //ROS_ERROR("%s",ex.what());
    return false;
  }
}

//Get last time the frame was detected
bool MissionControllerProcess::getTimeFromTF(std::string frame, double & time){
  tf::StampedTransform transform;
  try{
    listener.lookupTransform("odom", frame, ros::Time(0), transform);
    //Avoid getting same frame than before
    if (time == transform.stamp_.toSec()){
      return false;
    }else{
      time = transform.stamp_.toSec();
    }
    return true;
  }catch (tf::TransformException ex){
    //ROS_ERROR("%s",ex.what());
    return false;
  }
}

//Get sequence of times that target drone has been seen
//bool MissionControllerProcess::getSequenceFromTF(std::string frame, double & last_time, int & counter){
//    tf::StampedTransform transform;
//    try{
//        listener.lookupTransform("odom", frame, ros::Time(0), transform);
//        double current_time = transform.stamp_.toSec();

//        //Make sure we don't get the same frame that before
//        if (current_time == last_time) return true;

//        if (fabs(current_time-last_time) > time_threshold){
//            counter = 0;
//        }else{
//            counter += 1;
//        }
//        last_time = current_time;
//        return true;

//    }catch (tf::TransformException ex){
//        //ROS_ERROR("%s",ex.what());
//        return false;
//    }
//}

//Get sequence of times that target drone has been seen
void MissionControllerProcess::checkTimeOutCounterTF(std::string frame, int & counter, double time_threshold){

  tf::StampedTransform transform;
  try{
    listener.lookupTransform("odom", frame, ros::Time(0), transform);
    double tf_time = transform.stamp_.toSec();
    double time_now = ros::Time::now().toSec();
    //std::cout << "----------------------------------"<< std::endl;
    //std::cout << "frame : " << frame << std::endl;
    //std::cout << "time_threshold : " << time_threshold << std::endl;
    //std::cout << "abs(tf_time-time_now : " << fabs(tf_time-time_now)<< std::endl;
    if ((fabs(tf_time-time_now) > time_threshold) && (counter > 0)){

      std::cout << "************COUNTER RESET**********************"<< std::endl;
      std::cout << "************"<< frame <<"************"<< std::endl;

      counter = 0;
    }


  }catch (tf::TransformException ex){
    //ROS_ERROR("%s",ex.what());
    //std::cout << "frame : " << frame << std::endl;

  }
}

//Publish points
void MissionControllerProcess::publishPoints(std::vector<geometry_msgs::PoseStamped> points_list, float inflation_radius, float  gear_speed){
  mission_controller_mbzirc_process::SetPoints msg;


  msg.points_list = points_list;
  msg.inflation_radius = inflation_radius;
  msg.gear_speed = gear_speed;
  //planner_publisher.publish(msg);
  //sending only the first point not a bug
  mpc_points_publisher.publish(points_list[0]);

}

void MissionControllerProcess::publishExternalYaw(nav_msgs::Odometry external_yaw, Item::State state){
  double SLOW_K_YAW = 0.4;
  double FAST_K_YAW = 2.0;

  external_yaw.pose.pose.orientation.x = FAST_K_YAW;

  if(state == Item::State::SEARCH){
    external_yaw.pose.pose.orientation.x = SLOW_K_YAW;
  }
  if(state == Item::State::FOLLOW_DRONE_FAR){
    external_yaw.pose.pose.orientation.x = SLOW_K_YAW;
  }
  if(state == Item::State::RECOVER){
    if(far_counter_enable_flag){
      external_yaw.pose.pose.orientation.x = SLOW_K_YAW;
    }
  }
  external_yaw_publisher.publish(external_yaw);
}


//Detector mode callback
void MissionControllerProcess::detectorModeCallback(const std_msgs::Int32::ConstPtr &msg){
  //    std::cout << "************ counting *****************"<< msg->data << std::endl;

  //    std::cout << "counter_target_far : "<< counter_target_far<< std::endl;
  //    std::cout << "counter_target_close : "<< counter_target_close<< std::endl;
  //    std::cout << "counter_ball_close : "<< counter_ball_close<< std::endl;


  //    std::cout << "far_counter_enable_flag : "<< far_counter_enable_flag<< std::endl;
  //    std::cout << "close_counter_enable_flag : "<< close_counter_enable_flag<< std::endl;
  //    std::cout << "ball_counter_enable_flag : "<< ball_counter_enable_flag<< std::endl;



  //    if(msg->data){

  //        if(far_counter_enable_flag){
  //            counter_target_far += 1;
  //            counter_target_close = 0;}
  //    }else{

  //        if(close_counter_enable_flag){
  //            counter_target_close += 1;
  //            if(counter_target_close >= n_min_drone_close_detections){
  //                counter_target_far = 0;
  //        }

  //        }
  //    }



  if(msg->data == 1){

    if(far_counter_enable_flag){
      counter_target_far += 1;
      counter_target_close = 0;
      counter_ball_close=0;}


  }else if(msg->data == 0){

    if(close_counter_enable_flag){
      counter_target_close += 1;
      if(counter_target_close >= n_min_drone_close_detections){
        counter_target_far = 0;
        counter_ball_close = 0;
      }

    }
  }else if(msg->data == -1){

    if(ball_counter_enable_flag){
      counter_ball_close += 1;
    }

  }






}

//Command callback from visualizer
void MissionControllerProcess::commandsCallback(const std_msgs::Int32::ConstPtr &msg){
  // 0: Stop and reset
  // 1: Take off
  // 2: Land
  // 3: Next state
  // 4: Previous state
  switch(msg->data){
  case 0:
    std::cout << "STOP AND RESET"<< std::endl;
    break;
  case 1:
    std::cout << "TAKE OFF"<< std::endl;
    event_detector.wait_key_take_off = true;
    break;
  case 2:
    std::cout << "LAND"<< std::endl;
    current_state = Item::State::LAND;
    break;
  case 3:
    std::cout << "NEXT_STATE"<< std::endl;
    break;
  case 4:
    std::cout << "PREVIOUS_STATE"<< std::endl;
    break;
  }
}

// Callback gimbal from DJI_SDK
void MissionControllerProcess::gimbalAngleCallback(const geometry_msgs::Vector3Stamped &msg){
  // Found
  behavior_execution.setGimbalAngle(msg);
}

void MissionControllerProcess::takeOffPublisher(){
  droneMsgsROS::droneCommand msg;
  msg.command = droneMsgsROS::droneCommand::TAKE_OFF;
  high_level_command_publisher.publish(msg);
  ROS_WARN(" * * * * DRONE IS TAKING OFF  * * * * ");
}

void MissionControllerProcess::landPublisher(){
  droneMsgsROS::droneCommand msg;
  msg.command = droneMsgsROS::droneCommand::LAND;
  high_level_command_publisher.publish(msg);
  ROS_WARN(" * * * * DRONE IS LANDING  * * * * ");

}

void MissionControllerProcess::transformVectorStringToDouble(std::string & input, geometry_msgs::PoseStamped & output){
  boost::erase_all(input, " ");
  boost::erase_all(input, "[");
  boost::erase_all(input, "]");

  std::vector<std::string> tmp_vector;
  boost::split ( tmp_vector, input, boost::is_any_of(","));
  std::vector<double> tmp_vector_double;

  for(auto it = tmp_vector.begin(); it != tmp_vector.end() ; it ++ ){
    tmp_vector_double.push_back(stod(*it));
    //std::cout<< *it<<std::endl;


  }
  output.pose.position.x = tmp_vector_double[0];
  output.pose.position.y = tmp_vector_double[1];
  output.pose.position.z = tmp_vector_double[2];

}


void MissionControllerProcess::closeGripper(){
  std_msgs::Byte state_gripper;
  state_gripper.data = 1;
  gripper_state_publisher.publish(state_gripper);
}

void MissionControllerProcess::openGripper(){
  std_msgs::Byte state_gripper;
  state_gripper.data = 0;
  gripper_state_publisher.publish(state_gripper);

}

bool MissionControllerProcess::checkBallInGripper(){
  mission_controller_mbzirc_process::CheckCatch gripper_catch_srv;

  catch_ball_client.call(gripper_catch_srv);

  bool answer = gripper_catch_srv.response.catched;
  return answer;
}


void MissionControllerProcess::dynamicTunningCallback(mission_controller_mbzirc::MyParamsConfig &config, uint32_t level){
  double h1_slow = config.h1_slow;
  behavior_execution.h1_slow_DR = h1_slow;
  double h2_slow = config.h2_slow;
  behavior_execution.h2_slow_DR = h2_slow;
  double d1_slow = config.d1_slow;
  behavior_execution.d1_slow_DR = d1_slow;
  double d2_slow = config.d2_slow;
  behavior_execution.d2_slow_DR = d2_slow;


  double h1_fast = config.h1_fast;
  behavior_execution.h1_fast_DR = h1_fast;
  double h2_fast = config.h2_fast;
  behavior_execution.h2_fast_DR = h2_fast;
  double d1_fast = config.d1_fast;
  behavior_execution.d1_fast_DR = d1_fast;
  double d2_fast = config.d2_fast;
  behavior_execution.d2_fast_DR = d2_fast;



  double gear_speed_far = config.gear_speed_far;
  behavior_execution.gear_speed_far_DR = gear_speed_far;
  double gear_speed_recover = config.gear_speed_recover;
  behavior_execution.gear_speed_recover_DR = gear_speed_recover;
  double gear_speed_focus = config.gear_speed_focus;
  behavior_execution.gear_speed_focus_DR = gear_speed_focus;
  double gear_speed_search = config.gear_speed_search;
  behavior_execution.gear_speed_search_DR = gear_speed_search;
  double gear_speed_takeoff_and_land = config.gear_speed_takeoff_and_land;
  behavior_execution.gear_speed_takeoff_and_land_DR = gear_speed_takeoff_and_land;


  double close_follow_distance_XY_drone = config.close_follow_distance_XY_drone;
  behavior_execution.close_follow_distance_XY_drone_DR = close_follow_distance_XY_drone;
  double close_follow_distance_Z_drone = config.close_follow_distance_Z_drone;
  behavior_execution.close_follow_distance_Z_drone_DR = close_follow_distance_Z_drone;

  double close_follow_distance_XY_ball = config.close_follow_distance_XY_ball;
  behavior_execution.close_follow_distance_XY_ball_DR = close_follow_distance_XY_ball;
  double close_follow_distance_Z_ball = config.close_follow_distance_Z_ball;
  behavior_execution.close_follow_distance_Z_ball_DR = close_follow_distance_Z_ball;

  double inflation_radius_follow_close_drone = config.inflation_radius_follow_close_drone;
  behavior_execution.inflation_radius_follow_close_drone_DR = inflation_radius_follow_close_drone;

  double distance_threshold_catch = config.distance_threshold_catch;
  event_detector.distance_threshold_catch_DR = distance_threshold_catch;

  double gear_speed_catch_ball = config.gear_speed_catch_ball;
  behavior_execution.gear_speed_catch_ball_DR = gear_speed_catch_ball;
  double distance_behind_ball_catch = config.distance_behind_ball_catch;
  behavior_execution.distance_behind_ball_catch_DR = distance_behind_ball_catch;
  double offset_ball_catch = config.offset_ball_catch;
  behavior_execution.offset_ball_catch_DR = offset_ball_catch;
  double inflation_radius_ball_follow = config.inflation_radius_ball_follow;
  behavior_execution.inflation_radius_ball_follow_DR = inflation_radius_ball_follow;
  std::cout << "Dynamic reconfigure parameters update. CHECK IF YOU CHANGE ANYTHING YOU WANT TO SAVE" << std::endl;


}

